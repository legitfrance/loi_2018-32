Annexe 2. Mode de calcul du solde structurel

Le solde structurel est le solde qui serait observé si le PIB était égal à son potentiel. Il correspond au solde public corrigé des effets du cycle économique et son calcul repose donc sur l'écart entre le PIB effectif noté Y et le PIB potentiel noté Y*.
Côté dépense, seules les dépenses de chômage sont supposées cycliques. Le reste des dépenses sont supposées être structurelles, soit parce qu'elles sont de nature discrétionnaire, soit parce que leur lien avec la conjoncture est difficile à mesurer. Côté recettes, on suppose que tous les prélèvements obligatoires (impôt sur le revenu [IR] et contribution sociale généralisée [CSG], impôt sur les sociétés [IS], cotisations sociales et les autres prélèvements obligatoires) dépendent de la conjoncture tandis que le reste des recettes est supposé être indépendant à la position de l'économie dans le cycle.

Elasticités (4) à l'écart de production

<div><div align="center"><table border="1"><tr><th/><th>
Retenues depuis 2014</th></tr><tr><td align="left" valign="middle">
Impôt sur le revenu</td><td align="center" valign="middle">
1,86</td></tr><tr><td align="left" valign="middle">
CSG</td><td align="center" valign="middle">
1,86</td></tr><tr><td align="left" valign="middle">
Impôt sur les sociétés</td><td align="center" valign="middle">
2,76</td></tr><tr><td align="left" valign="middle">
Cotisations sociales</td><td align="center" valign="middle">
0,63</td></tr><tr><td align="left" valign="middle">
Autres prélèvements obligatoires (dont TVA)</td><td align="center" valign="middle">
1,00</td></tr><tr><td align="left" valign="middle">
Dépenses chômage</td><td align="center" valign="middle">
- 3,23</td></tr></table></div></div>
Pour chaque catégorie de prélèvements obligatoires R, la composante structurelle Rs peut s'écrire en fonction de l'élasticité conventionnelle θ à l'écart de production OG :
Le total des recettes structurelles est donc obtenu comme la somme des recettes structurelles, calculées Rs (pour les quatre catégories de prélèvements obligatoires cycliques : IR et CSG, IS, cotisations sociales et autres prélèvements obligatoires), additionnée au reste des recettes. Les dépenses structurelles s'obtiennent comme la différence entre les dépenses effectives et les dépenses structurelles liées au chômage, Dscho. Celles-ci sont déterminées de la même manière que pour les recettes structurelles, en fonction de l'élasticité conventionnelle ν des dépenses de chômage à l'écart de production.
La différence entre les dépenses structurelles et les recettes structurelles constitue le solde structurel Ss. Enfin, le ratio du solde structurel au PIB potentiel en valeur retient le déflateur du PIB.
