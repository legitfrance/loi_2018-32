L'objectif à moyen terme des administrations publiques mentionné au b du 1 de l'article 3 du traité sur la stabilité, la coordination et la gouvernance au sein de l'Union économique et monétaire, signé à Bruxelles le 2 mars 2012, est fixé à - 0,4 % du produit intérieur brut potentiel.
Dans le contexte macroéconomique et selon les hypothèses et les méthodes retenues pour établir la programmation, décrits dans le rapport mentionné à l'article 1er de la présente loi, l'objectif d'évolution du solde structurel des administrations publiques, défini au rapport annexé à la présente loi, s'établit, conformément aux engagements européens de la France, comme suit :

(En %)

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left">
Solde structurel</td><td align="right">
- 2,2</td><td align="right">
- 2,1</td><td align="right">
- 1,9</td><td align="right">
- 1,6</td><td align="right">
- 1,2</td><td align="right">
- 0,8</td></tr><tr><td align="left">
Ajustement structurel</td><td align="right">
0,3</td><td align="right">
0,1</td><td align="right">
0,3</td><td align="right">
0,3</td><td align="right">
0,4</td><td align="right">
0,4</td></tr></table></div></div>
