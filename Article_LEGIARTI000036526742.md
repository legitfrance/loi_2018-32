Annexe 6. Table de passage entre les dispositions de la loi organique relative à la programmation et à la gouvernance des finances publiques et le présent rapport annexé
Table de passage entre les dispositions de la loi organique relative à la programmation et à la gouvernance des finances publiques et le présent rapport annexé

<div><div align="center"><table border="1"><tr><td align="left" valign="middle">
LOI ORGANIQUE relatif à la programmation et à la gouvernance des finances publiques (article 5) prévoit que le rapport annexé contienne les éléments suivants :</td><td align="center" valign="middle">
PARTIES CORRESPONDANTES du rapport annexé</td></tr><tr><td align="left" valign="middle">
1° Les hypothèses et les méthodes retenues pour établir la programmation</td><td align="left" valign="middle">
Ensemble du rapport, notamment partie I sur le contexte macroéconomique</td></tr><tr><td align="left" valign="middle">
2° Pour chacun des exercices de la période de la programmation, les perspectives de recettes, de dépenses, de solde et d'endettement des administrations publiques et de chacun de leurs sous-secteurs, exprimées selon les conventions de la comptabilité nationale</td><td align="left" valign="middle">
Parties II et III : La trajectoire des finances publiques et analyse par sous-secteur</td></tr><tr><td align="left" valign="middle">
3° Pour chacun des exercices de la période de la programmation, l'estimation des dépenses d'assurance vieillesse et l'estimation des dépenses d'allocations familiales</td><td align="left" valign="middle">
Partie III C : Dépenses d'assurance vieillesse et d'allocations familiales</td></tr><tr><td align="left" valign="middle">
4° Pour chacun des exercices de la période de la programmation, les perspectives de recettes, de dépenses et de solde des régimes complémentaires de retraite et de l'assurance chômage, exprimées selon les conventions de la comptabilité nationale</td><td align="left" valign="middle">
Partie III C : Dépenses des régimes complémentaires de retraite et de l'assurance chômage</td></tr><tr><td align="left" valign="middle">
5° Les mesures de nature à garantir le respect de la programmation</td><td align="left" valign="middle">
Partie III (la trajectoire des finances publiques par sous-secteur)</td></tr><tr><td align="left" valign="middle">
6° Toute autre information utile au contrôle du respect des plafonds et objectifs mentionnés aux 1° et 2° de l'article 2, notamment les principes permettant de comparer les montants que la loi de programmation des finances publiques prévoit avec les montants figurant dans les lois de finances de l'année et les lois de financement de la sécurité sociale de l'année</td><td align="left" valign="middle">
Partie III (la trajectoire des finances publiques par sous-secteur)</td></tr><tr><td align="left" valign="middle">
7° Les projections de finances publiques à politiques inchangées, au sens de la directive 2011/85/UE du Conseil, du 8 novembre 2011, sur les exigences applicables aux cadres budgétaires des Etats membres, et la description des politiques envisagées pour réaliser l'objectif à moyen terme au regard de ces projections</td><td align="left" valign="middle">
Partie III.C</td></tr><tr><td align="left" valign="middle">
8° Le montant et la date d'échéance des engagements financiers significatifs de l'Etat en cours n'ayant pas d'implication immédiate sur le solde structurel</td><td align="left" valign="middle">
Partie II 5 Encadré « Mesures exceptionnelles et temporaires - Hypothèses retenues dans la programmation »</td></tr><tr><td align="left" valign="middle">
9° Les modalités de calcul de l'effort structurel mentionné à l'article 1er, la répartition de cet effort entre chacun des sous-secteurs des administrations publiques et les éléments permettant d'établir la correspondance entre la notion d'effort structurel et celle de solde structurel</td><td align="left" valign="middle">
Partie II D : Evolution du solde structurel et de l'effort structurel des administrations publiques.)</td></tr><tr><td align="left" valign="middle">
10° Les hypothèses de produit intérieur brut potentiel retenues pour la programmation des finances publiques. Le rapport présente et justifie les différences éventuelles par rapport aux estimations de la Commission européenne</td><td align="left" valign="middle">
Partie I (les hypothèses de produit intérieur brut potentiel et la justification des écarts par rapport aux estimations de la Commission européenne)</td></tr><tr><td align="left" valign="middle">
11° Les hypothèses ayant permis l'estimation des effets de la conjoncture sur les dépenses et les recettes publiques, et notamment les hypothèses d'élasticité à la conjoncture des différentes catégories de prélèvements obligatoires et des dépenses d'indemnisation du chômage. Le rapport présente et justifie les différences éventuelles par rapport aux estimations de la Commission européenne</td><td align="left" valign="middle">
Annexe 2</td></tr><tr><td align="left" valign="middle">
12° Les modalités de calcul du solde structurel annuel mentionné à l'article 1er</td><td align="left" valign="middle">
Annexe 2</td></tr><tr><td align="left" valign="middle">
Ce rapport présente également la situation de la France au regard des objectifs stratégiques européens</td><td align="left" valign="middle">
Partie II A</td></tr></table></div></div><p class="note">
(1) En données CVS-CJO. Les données brutes pourraient être 0,15 pt en-dessous d'après l'INSEE en 2017.
(2) Recommandation du Conseil, 22 mai 2017.
(3) Formation brute de capital fixe et acquisition moins cession d'actifs non financiers non produits.
(4) Plus précisément semi-élasticité à l'écart de production
(5) Cf. “Manual on Government deficit and debt” (MGDD) edition 2016, §II.3 pp.93 et 94.
(6) Cf. MGDD 2016, §II.2.26 p.90 “The time of recording of the expenditure should be when government recognizes the claim for its whole amount […]”.
(7) Une difficulté vient du fait que la bascule transforme un crédit d'impôt de type « Subvention » (D.3) en une moindre recette de « Cotisation sociales » (D.6), et que les deux opérations ont une date d'enregistrement recommandée différente en comptabilité nationale (contemporain aux salaires pour le D.6, décalé de 1 an pour le crédit d'impôt en D.3). Cependant, dans les deux cas, le fait générateur économique du transfert correspond aux salaires versés, qui n'est pas modifié par la bascule.</p>
