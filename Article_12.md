I. - L'objectif de dépenses des régimes obligatoires de base de sécurité sociale ne peut, à périmètre constant, excéder les montants suivants, exprimés en pourcentage du produit intérieur brut et en milliards d'euros courants :

<div><div align="center"><table border="1"><tr><th/><th>
2018</th><th>
2019</th><th>
2020</th></tr><tr><td align="left">
En % du PIB</td><td align="center" valign="middle">
21,2</td><td align="center" valign="middle">
21,0</td><td align="center" valign="middle">
20,8</td></tr><tr><td align="left">
En milliards d'euros courants</td><td align="center" valign="middle">
497,7</td><td align="center" valign="middle">
508,1</td><td align="center" valign="middle">
519,1</td></tr></table></div></div>
II. - L'objectif national de dépenses d'assurance maladie de l'ensemble des régimes obligatoires de base de sécurité sociale ne peut, à périmètre constant, conformément à la méthodologie décrite dans le rapport annexé à la présente loi, excéder les montants suivants, exprimés en milliards d'euros courants :

<div><div align="center"><table border="1"><tr><th>
2018</th><th>
2019</th><th>
2020</th></tr><tr><td align="center">
195,2</td><td align="center">
199,7</td><td align="center">
204,3</td></tr></table></div></div>
