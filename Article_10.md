L'incidence, en 2022, des schémas d'emplois exécutés de 2018 à 2022 pour l'Etat et ses opérateurs est inférieure ou égale à -50 000 emplois exprimés en équivalents temps plein travaillé.
