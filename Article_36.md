A modifié les dispositions suivantes :

<blockquote>- LOI n° 2014-1653 du 29 décembre 2014
<blockquote>
Art. 12,  Art. 30,  Art. 1,  Art. 2,  Art. 3,  Art. 4,  Art. 5,  Art. 6,  Art. 7,  Art. 8,  Art. 9,  Art. 10,  Art. 11,  Art. 13,  Art. 14,  Art. 15,  Art. 16,  Art. 17,  Art. 18,  Art. 19,  Art. 20,  Art. 21,  Art. 22,  Art. 23,  Art. 24,  Art. 25,  Art. 27,  Art. 29,  Art. 31,  Art. 33,  Art. 35,  Sct. Chapitre Ier : Les objectifs généraux des finances publiques,  Sct. Chapitre III : L'évolution des dépenses de l'Etat sur la période 2015-2017,  Sct. Chapitre IV : Les recettes publiques et le pilotage des niches fiscales et sociales,  Sct. Chapitre Ier : Revues de dépenses et évaluation des dépenses fiscales et niches sociales,  Sct. Annexe,  Art. Rapport,  Sct. Annexes,  Art. Annexe 1,  Art. Annexe 2,  Art. Annexe 3,  Art. Annexe 4,  Art. Annexe 5,  Art. Annexe 6
</blockquote>
</blockquote><div>
</div>

La présente loi sera exécutée comme loi de l'Etat.
