I.-A modifié les dispositions suivantes :

<blockquote>- LOI n° 2014-1653 du 29 décembre 2014
<blockquote>
Art. 34
</blockquote>
</blockquote>

II.-L'article 34 de la loi n° 2014-1653 du 29 décembre 2014 de programmation des finances publiques pour les années 2014 à 2019, dans sa rédaction résultant de la présente loi, s'applique aux contrats dont l'avis d'appel public à la concurrence est publié à compter du 1er janvier 2018.
