Le Gouvernement transmet chaque année au Parlement une présentation précise et détaillée des deux agrégats de dépenses de l'Etat, prévus à l'article 9 de la présente loi. Cette présentation décompose, à périmètre constant, les différents éléments de ces deux agrégats, pour l'exercice antérieur, l'exercice en cours et l'exercice à venir.
Elle précise le montant :
1° Des crédits du budget général hors charge de la dette, pensions, investissements d'avenir et remboursements et dégrèvements ;
2° Des impositions de toutes natures plafonnées en application de l'article 46 de la loi n° 2011-1977 du 28 décembre 2011 de finances pour 2012 ;
3° Des dépenses des comptes d'affectation spéciale prises en compte dans la norme de dépenses pilotables ;
4° Des dépenses du compte de concours financier « Avances à l'audiovisuel public » ;
5° Du prélèvement sur recettes au profit de l'Union européenne ;
6° Du prélèvement sur recettes au profit des collectivités territoriales ;
7° Des dépenses des comptes d'affectation spéciale prises en compte dans le seul objectif de dépenses totales de l'Etat ;
8° Des dépenses d'investissements d'avenir ;
9° De la charge de la dette ;
10° De la fraction de taxe sur la valeur ajoutée affectée aux régions, au département de Mayotte et aux collectivités territoriales de Corse, de Martinique et de Guyane telle que définie à l'article 149 de la loi n° 2016-1917 du 29 décembre 2016 de finances pour 2017 ;
11° Des retraitements de flux internes au budget de l'Etat.
Cette présentation est rendue publique en même temps que le rapport prévu à l'article 50 de la loi organique n° 2001-692 du 1er août 2001 relative aux lois de finances.
La liste des retraitements de flux internes au budget de l'Etat ainsi que l'inventaire des programmes des comptes spéciaux intégrés à la norme de dépenses pilotables, d'une part, et à l'objectif de dépenses totales de l'Etat, d'autre part, sont indiqués chaque année en annexe au projet de loi de finances de l'année et en annexe au projet de loi de règlement des comptes et d'approbation du budget.
En cas d'exclusion de certaines dépenses du périmètre de la norme en raison de leur caractère exceptionnel, les critères ayant conduit le Gouvernement à retenir le caractère exceptionnel des dépenses considérées sont précisés en annexe au projet de loi de finances de l'année.
