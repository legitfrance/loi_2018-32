ANNEXE
RAPPORT ANNEXÉ À LA LOI DE PROGRAMMATION DES FINANCES PUBLIQUES 2018-2022

I. - Le contexte macroéconomique s'améliore, mais les finances publiques restent structurellement dégradées.
A. - Les perspectives à court terme (2017-2018).
B. - Les perspectives à moyen terme (2019-2022).
II. - Dès 2018 et tout au long du quinquennat, le Gouvernement engage une transformation profonde des structures de l'action publique qui permettra le respect de nos engagements de finances publiques.
A. - Conformément à nos engagements européens, la politique du Gouvernement vise un retour vers l'équilibre structurel et une réduction du ratio de dette publique.
B. - Le taux de prélèvements obligatoires sera abaissé de 1 point sur le quinquennat pour favoriser la croissance et l'emploi.
C. - Une baisse de plus de 3 points de la dépense publique à horizon 2022.
D. - A l'horizon du quinquennat, l'effort structurel portera sur la dépense et permettra la diminution du ratio de dette publique.
E. - Une transformation de l'action et de la gestion publique.
III. - L'effort sera équitablement réparti entre les sous-secteurs des administrations publiques.
A. - La trajectoire de l'Etat.
B. - La trajectoire des organismes divers d'administration centrale.
C. - La trajectoire des administrations de sécurité sociale.
D. - La trajectoire des administrations publiques locales.
E. - Si la législation et les politiques de finances publiques n'étaient pas réformées, le déficit public se résorberait plus lentement et la dette ne décroîtrait pas sur le quinquennat.
Annexes :
Annexe 1 : Principales définitions.
Annexe 2 : Mode de calcul du solde structurel.
Annexe 3 : Périmètre des mesures ponctuelles et temporaires à exclure de la mesure du solde structurel.
Annexe 4 : Précisions méthodologiques concernant le budget quinquennal.
Annexe 5 : Précisions méthodologiques concernant la mesure de la croissance des dépenses au sein de l'objectif national des dépenses d'assurance maladie (Ondam).
Annexe 6 : Table de passage entre les dispositions de la loi organique relative à la programmation et à la gouvernance des finances publiques et le présent rapport annexé.

I. - Le contexte macroéconomique s'améliore, mais les finances publiques restent structurellement dégradées
A. - Les perspectives à court terme (2017-2018)

L'économie française est sur une trajectoire de reprise plus vigoureuse depuis fin 2016 : la croissance s'établirait à + 1,7 % en 2017 et en 2018 (1). Alors que la crise de la dette en zone euro avait fortement pesé sur l'activité économique en 2012 et 2013, celle-ci a retrouvé un rythme de croissance plus solide, proche de 1 % entre 2014 et 2016, mais toujours modéré au regard de la croissance potentielle de l'économie française, estimée à environ 1,25 %. A partir de l'automne 2016, l'activité a accéléré pour atteindre une progression trimestrielle autour de 0,5 % au dernier trimestre 2016 et au premier semestre 2017, dans un contexte où les entreprises et les ménages sont nettement plus optimistes sur la situation et les perspectives économiques depuis le second trimestre 2017.
L'activité serait aussi soutenue par une plus forte croissance dans les pays avancés et émergents. La demande mondiale adressée à la France accélèrerait fortement en 2017 et progresserait encore nettement en 2018 mais sans accélérer, car l'accélération de l'activité aux Etats-Unis et de l'investissement privé en Allemagne serait compensée par le ralentissement économique attendu en Chine et au Royaume-Uni. Ce regain de dynamisme extérieur se traduirait par des exportations qui croîtraient en 2017-2018 de manière plus vigoureuse qu'en 2016.
La demande intérieure marquerait le pas en 2017 puis accélérerait en 2018 : en particulier, après un dynamisme marqué en 2016, la consommation des ménages ralentirait en 2017, en partie en raison d'un 1er trimestre décevant, lié à de faibles dépenses en énergie. L'investissement des entreprises resterait allant à horizon de la prévision, soutenu par la demande tant extérieure qu'intérieure et les mesures fiscales de Gouvernement, tandis que l'investissement des ménages serait soutenu par un contexte économique (dynamisme du pouvoir d'achat et de l'emploi) et financier favorable en dépit de la remontée des taux d'intérêt.
Ce scénario de croissance est proche des dernières anticipations des autres prévisionnistes : l'OCDE anticipe une croissance de + 1,7 % en 2017 puis + 1,6 % en 2018 dans le rapport EDR France publié le 14 septembre. Le consensus des économistes de marché s'établit en septembre à + 1,6 % en 2017 comme en 2018. L'INSEE attendait + 1,6 % pour 2017 dans sa note de conjoncture de juin, le FMI prévoyait + 1,5 % en 2017 et + 1,7 % en 2018 dans ses prévisions de juillet, et la Banque de France tablait sur + 1,6 % les deux années en juillet. Pour sa part, la Commission européenne n'a pas encore revu sa prévision publiée au printemps dernier : + 1,4 % en 2017 puis + 1,7 % en 2018.
Cette prévision est soumise à de nombreux aléas. L'environnement international est incertain, en particulier en ce qui concerne les négociations sur la sortie du Royaume-Uni de l'UE et l'orientation de la politique budgétaire américaine. Les évolutions du pétrole et du change sont également sources d'incertitudes. L'investissement des entreprises pourrait être moins dynamique qu'attendu, si celles-ci souhaitaient réduire leur endettement. A l'inverse, il pourrait être plus dynamique si la reprise économique était plus marquée. Le dynamisme de l'emploi et le regain de confiance des ménages pourraient soutenir la consommation plus qu'escompté. Toutefois, l'investissement des ménages pourrait être moins élevé si le ralentissement des mises en chantier observé depuis quelques mois s'accentuait.

B. - Les perspectives à moyen terme (2019-2022)

A moyen-terme, les projections de finances publiques reposent sur une hypothèse de stabilisation de la croissance à + 1,7 % de 2019 à 2021, puis à + 1,8 % en 2022.
Cette projection s'appuie sur des hypothèses prudentes de croissance potentielle et d'écarts de production.
La croissance potentielle s'établirait à 1,25 % sur 2017-2020, 1,3 % en 2021 et 1,35 % en 2022. Cette estimation est proche de celle de la Commission (voir encadré) et repose sur une tendance de la productivité plus faible qu'avant-crise, en raison notamment du ralentissement des effets du progrès technique au niveau mondial.
La croissance potentielle augmenterait à l'horizon 2022 grâce aux effets positifs des réformes structurelles qui seront mises en œuvre lors du quinquennat, notamment pour favoriser la formation et l'apprentissage, réformer l'indemnisation du chômage, baisser le coin socio-fiscal (bascule des cotisations salariales sur la CSG), moderniser le code du travail, et soutenir l'investissement productif (abaissement de l'impôt sur les sociétés (IS) à 25 %, mise en place du prélèvement forfaitaire unique, réforme de l'impôt de solidarité sur la fortune (ISF) qui ne pèsera plus sur les valeurs mobilières) et l'innovation (pérennisation du crédit d'impôt-recherche).
Le scénario de moyen-terme est celui d'un redressement progressif de l'écart de production, de - 1,5 % en 2016 à + 1,1 % en 2022. Après huit années d'écart de production négatif (entre 2012 et 2019), il serait positif à partir de 2020 mais resterait inférieur aux niveaux observés avant la crise de 2008-2009 ou au début des années 2000.
Le scénario macroéconomique retenu pour la programmation pluriannuelle fait en outre l'hypothèse d'un retour progressif des prix vers des niveaux cohérents avec la cible de la Banque centrale européenne.

Tableau : principales hypothèses du scénario macroéconomique 2018-2022 (*)

<div><div align="center"><table border="1"><tr><th/><th>
2016</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
PIB (**)</td><td align="center" valign="middle">
1,1</td><td align="center" valign="middle">
1,7</td><td align="center" valign="middle">
1,7</td><td align="center" valign="middle">
1,7</td><td align="center" valign="middle">
1,7</td><td align="center" valign="middle">
1,7</td><td align="center" valign="middle">
1,8</td></tr><tr><td align="left" valign="middle">
Déflateur de PIB</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,8</td><td align="center" valign="middle">
1,1</td><td align="center" valign="middle">
1,25</td><td align="center" valign="middle">
1,5</td><td align="center" valign="middle">
1,75</td><td align="center" valign="middle">
1,75</td></tr><tr><td align="left" valign="middle">
Indice des prix à la consommation hors tabac</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
1,0</td><td align="center" valign="middle">
1,0</td><td align="center" valign="middle">
1,1</td><td align="center" valign="middle">
1,4</td><td align="center" valign="middle">
1,75</td><td align="center" valign="middle">
1,75</td></tr><tr><td align="left" valign="middle">
Masse salariale privée</td><td align="center" valign="middle">
2,4</td><td align="center" valign="middle">
3,3</td><td align="center" valign="middle">
3,1</td><td align="center" valign="middle">
3,2</td><td align="center" valign="middle">
3,6</td><td align="center" valign="middle">
3,8</td><td align="center" valign="middle">
3,8</td></tr><tr><td align="left" valign="middle">
Croissance potentielle</td><td align="center" valign="middle">
1,2</td><td align="center" valign="middle">
1,25</td><td align="center" valign="middle">
1,25</td><td align="center" valign="middle">
1,25</td><td align="center" valign="middle">
1,25</td><td align="center" valign="middle">
1,30</td><td align="center" valign="middle">
1,35</td></tr><tr><td align="left" valign="middle">
PIB potentiel (en Md€ 2010)</td><td align="center" valign="middle">
2 154</td><td align="center" valign="middle">
2 181</td><td align="center" valign="middle">
2 209</td><td align="center" valign="middle">
2 236</td><td align="center" valign="middle">
2 264</td><td align="center" valign="middle">
2 294</td><td align="center" valign="middle">
2 325</td></tr><tr><td align="left" valign="middle">
Ecart de production (en % du PIB)</td><td align="center" valign="middle">
- 1,5</td><td align="center" valign="middle">
- 1,1</td><td align="center" valign="middle">
- 0,7</td><td align="center" valign="middle">
- 0,2</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,6</td><td align="center" valign="middle">
1,1</td></tr><tr><td align="left" colspan="8" valign="middle">
Note : (*) Données exprimées en taux d'évolution annuelle, sauf précision contraire.
(**) Données corrigées des jours ouvrables.</td></tr></table></div></div><div><div align="center"><table border="1"><tr><td align="left" valign="middle">
Encadré : Explication des écarts entre la croissance potentielle de la LPFP et celle de la Commission européenne
Le scénario de croissance potentielle retenu - 1,25 % sur 2018-2020 - est proche de celui de la Commission, qui est de 1,2 % en moyenne sur la même période dans ses prévisions de printemps 2017. L'écart de production retenu en 2016 (- 1,5 %) est également proche de l'estimation de la Commission Européenne (- 1,3 %).
Les hypothèses de population active sont différentes car la Commission utilise l'exercice européen de projection de population à moyen terme (« Europop »), tandis que les estimations du Gouvernement s'appuient sur les projections les plus récentes de l'Insee pour la France, publiées le 9 mai 2017, moins dynamiques en fin de période. En revanche, le chômage structurel retenu est celui de la Commission européenne (estimé comme un NAWRU, c'est-à-dire un taux de chômage ne faisant pas accélérer les coûts salariaux unitaires réels).
Les prévisions d'investissement sous-jacentes à l'accumulation du capital sont proches entre le scénario du Gouvernement et celui de la Commission ; le scénario de capital retenu repose sur un taux d'investissement par rapport au capital existant égal à sa moyenne 2010-2016.
En revanche, la Commission retient une progression de la productivité tendancielle moins allante que le Gouvernement. Cela s'explique notamment par le traitement de la crise : malgré la prise en compte de la partie cyclique du ralentissement de la productivité liée à la sous-utilisation des capacités de production, la Commission ne retient pas de choc en niveau. Cependant, la Commission retient un effet durable sur la progression de la productivité tendancielle et donc sur la croissance potentielle.
Le Gouvernement a opéré un choix différent et retenu un choc en niveau sur la productivité pendant la crise et un ralentissement de tendance post-crise. En raison de l'introduction de ce choc en niveau sur la période 2008-2010, le ralentissement de la productivité est moins marqué dans les estimations du Gouvernement que dans celles de la Commission (0,6/0,7 % par an sur la période 2018-2022 contre 0,4/0,5 % pour la Commission). La croissance de la productivité globale des facteurs (PGF) tendancielle retenue est proche et légèrement supérieure à celle observée sur la période 2011-2016. Les réformes structurelles mises en œuvre en fin de quinquennat précédent, en particulier les réformes pro-concurrentielles et de simplification, ainsi que les réformes sur le marché du travail et le dialogue social sont favorables à la productivité. Mais, du fait d'un phasage tardif au cours du précédent quinquennat, en seconde moitié voire en toute fin, leurs pleins effets ne se sont pas encore totalement matérialisés. Par ailleurs le rattrapage en cours sur l'intégration des technologies numériques par les entreprises, ainsi que la remontée des dépenses privées de R&amp;amp ;D avec la montée en puissance du CIR sont également des facteurs de soutien.
Le choix de ne pas retenir le scénario de la Commission européenne s'explique donc d'une part par la volonté de prendre en compte les dernières projections de population active de l'Insee et d'autre part par le choix de calcul de la tendance de la PGF.</td></tr></table></div></div>
II. - Dès 2018 et tout au long du quinquennat, le Gouvernement engage une transformation profonde des structures de l'action publique qui permettra le respect de nos engagements de finances publiques
A. - Conformément à nos engagements européens, la politique du Gouvernement vise un retour vers l'équilibre structurel et une réduction du ratio de dette publique

Le Gouvernement a pris des mesures fortes de maîtrise des dépenses publiques dès son arrivée, afin de respecter l'objectif de 2,9 % de déficit, de sorte à permettre la sortie de la procédure pour déficit public excessif ouverte à l'encontre de la France depuis 2009. Le Conseil européen du 5 mars 2015 a recommandé à la France de corriger son déficit excessif en 2017 au plus tard. La chronique de déficits publics prévue par la LPFP, présentant un retour durable du déficit public sous 3,0 % du PIB à partir de 2017, permet de préparer une sortie de la procédure en 2018. Celle-ci pourrait être proposée par la Commission et décidée par le Conseil, sur la base des données d'exécution de l'année 2017, qui seront notifiées par Eurostat en avril 2018, et des prévisions de printemps de la Commission pour les déficits publics des années 2018 et 2019.
Pour la suite de la trajectoire, et comme prévu dans le traité sur la stabilité, la coordination et la gouvernance au sein de l'Union économique et monétaire (TSCG), l'objectif à moyen terme d'équilibre structurel des finances publiques (OMT) est fixé de manière spécifique par chaque Etat, avec une limite inférieure de - 0,5 % de PIB potentiel. Conformément à l'article 1er de la loi organique n° 2012-1403 du 17 décembre relative à la programmation et à la gouvernance des finances publiques, il revient à la loi de programmation des finances publiques de fixer l'OMT visé par le Gouvernement. L'article 2 de la présente loi de programmation fixe l'OMT à - 0,4 % du PIB potentiel. L'OMT est inchangé par rapport à la précédente LPFP.
Une nouvelle estimation menée à l'occasion du débat d'orientation des finances publiques 2017 a conduit à revoir le niveau de l'écart de production par rapport au cadrage macroéconomique retenu par le précédent Gouvernement. Ainsi, pour l'année 2016, le niveau du solde structurel a été dégradé, passant de - 1,5 % à - 2,5 %. Cela signifie que le déficit public est en réalité moins dû à la situation conjoncturelle, qu'à un niveau de dépenses structurellement élevé. Cette révision a pour effet, toutes choses égales par ailleurs, d'allonger la durée de consolidation budgétaire permettant d'atteindre l'OMT. La trajectoire de la présente loi de programmation permet une amélioration continue du solde structurel jusqu'en 2022, où il s'établira à - 0,8 % de PIB potentiel, ce qui permettra d'atteindre l'OMT en 2023.
Une telle trajectoire tient compte de la nécessité de soutenir la reprise en cours, alors que la France est l'un des pays de la zone euro dont l'écart de production est le plus creusé, d'après les dernières estimations de la Commission européenne. En outre, cette trajectoire de consolidation progressive des finances publiques permettra de soutenir le déploiement des réformes structurelles engagées par le Gouvernement, qui généreront des effets positifs à moyen terme sur la soutenabilité des finances publiques et le potentiel d'activité. Le rythme de réduction du déficit public prévu sur la durée du quinquennat s'inscrit donc dans la stratégie globale de politique économique du Gouvernement et permettra de combler la plus large partie de l'écart entre le déficit structurel actuel et l'objectif de moyen terme au cours du quinquennat.
Dans le même temps, les efforts consentis permettront de réduire la dette publique de manière significative dans les cinq années à venir. Ainsi, le ratio de dette sur PIB, qui atteint 96,3 % en 2016 s'établira à 91,4 % en 2022 et sera inscrit à cet horizon sur une pente décroissante.
La conformité de cette trajectoire aux règles budgétaires européennes sera évaluée chaque année sur la base des données notifiées, dans le cadre d'une analyse d'ensemble de la Commission, qui tient compte de la situation économique de chaque pays. Pour l'année 2018, cette dernière a précisé que l'évaluation du respect de l'ajustement recommandé sera faite à la lumière de la position des Etats membres dans le cycle économique, afin de tenir compte notamment de la nécessité relative de soutenir la reprise économique en cours. L'ajustement structurel 2018 serait de 0,1 pt de PIB, dans un contexte d'écart de production encore creusé et de mise en œuvre de réformes structurelles majeures (marché du travail, fiscalité, investissement, logement, climat…). L'ensemble de ces facteurs sont pris en compte dans les règles budgétaires européennes pour évaluer le caractère approprié de l'ajustement structurel par rapport à la réalité économique et aux réformes de chaque pays. L'ajustement prévu par la France pour 2018 permettrait de ne pas dévier significativement de la recommandation qui lui a été adressée par le Conseil. A partir de 2019, l'ajustement structurel sera en moyenne de 0,35 point de PIB potentiel par an, conformément à nos engagements européens (2).

B. - Le taux de prélèvements obligatoires sera abaissé de 1 point sur le quinquennat pour favoriser la croissance et l'emploi

La pression fiscale qui pèse sur les entreprises et les ménages français, entravant la demande et l'initiative privées, sera relâchée. La baisse des prélèvements obligatoires se poursuivra avec une baisse d'un point de PIB d'ici 2022, dont plus de 10 milliards d'euros de baisse d'ici à fin 2018, bénéficiant à la fois aux ménages et aux entreprises. Sur la période de programmation, le taux de prélèvements obligatoires sera ramené de 44,7 % en 2017 à 43,7 % en 2022. Cette baisse répond à trois choix stratégiques du Gouvernement. Le premier est de soutenir immédiatement la croissance et de favoriser l'emploi et le pouvoir d'achat en récompensant le travail, en engageant ces baisses dès le 1er janvier 2018, le deuxième d'améliorer la compétitivité et de libérer l'activité des entreprises, et le troisième de soutenir l'investissement privé dans les entreprises qui prennent des risques, qui innovent et qui créent les emplois de demain.
Cette baisse de la fiscalité portera en grande partie sur les ménages et bénéficiera en priorité aux actifs des classes moyennes et aux ménages modestes. Pour cela, les cotisations salariales maladie et chômage des salariés du secteur privé seront supprimées et financée par une hausse partielle de CSG dont l'assiette est plus large, permettant ainsi d'élargir les bases fiscales notamment s'agissant du financement de la protection sociale afin qu'il ne pèse pas uniquement sur le coût du travail. Outre le gain net de pouvoir d'achat que représentent ces baisses pour les actifs, elles stimuleront à la fois la demande et l'offre de travail en réduisant le coin fiscalo-social sur les salaires, en complément de la revalorisation de la prime d'activité. Par ailleurs, 80 % des ménages seront progressivement exonérés d'ici 2020 de la taxe d'habitation, avec un premier allègement de 3 milliards d'euros dès 2018, pour un gain de pouvoir d'achat et une plus grande équité fiscale (2).
Afin de renforcer la compétitivité des entreprises, l'activité et l'attractivité de notre économie, la fiscalité des entreprises sera allégée et simplifiée. Le taux facial de l'impôt sur les sociétés sera réduit, par étapes, à 25 % d'ici 2022. Cette baisse permettra de converger vers la moyenne européenne et de réduire le coût du capital, stimulant ainsi l'investissement à long terme. Par ailleurs, le CICE sera transformé en allègement pérenne de cotisations patronales dès 2019 - date privilégiée afin de garantir, dans un premier temps, un retour durable du déficit public sous 3 % du PIB - afin de simplifier le dispositif existant et de soutenir dans la durée l'emploi et la compétitivité des entreprises françaises. Cette transformation du CICE est également un gage de stabilité pour les entreprises, en particulier pour les plus petites d'entre elles. Cette baisse de cotisations sécurisera non seulement l'effort entrepris jusqu'ici pour aider les entreprises à restaurer leurs marges, mais elle soutiendra aussi la demande de travail peu qualifié du fait d'un ciblage plus important qu'aujourd'hui au niveau du salaire minimum.
Pour stimuler l'investissement productif, risqué et innovant, l'Impôt de solidarité sur la fortune (ISF) sera transformé en Impôt sur la Fortune Immobilière (IFI) dès 2018 et un taux de prélèvement unique de 30 % sur les revenus de l'épargne sera également instauré en 2018, incluant les prélèvements sociaux. Dans un contexte de transition technologique profonde, le besoin en capital est encore plus important qu'hier et alléger sa fiscalité est devenu indispensable. Ces différentes mesures s'inscrivent également dans une logique de convergence européenne, puisque les taxes sur le capital ont, en France, un poids particulièrement élevé par rapport à nos partenaires européens. Outre le fait que ces mesures participent à l'effort de réduction du coût du capital, elles réorienteront l'épargne nationale vers le financement des entreprises.
Afin d'accélérer la conversion écologique de notre économie, la hausse de la fiscalité du carbone sera amplifiée et la convergence de la fiscalité du diesel sur celle de l'essence sera pleinement effective à l'horizon du quinquennat. Ceci permettra aux acteurs économiques d'internaliser le coût social généré par l'usage des énergies fossiles, réduisant ainsi nos émissions de CO2. La France progressera ainsi en matière de poids des recettes reposant sur la fiscalité environnementale. Des mesures budgétaires comme la généralisation du chèque-énergie et la mise en place d'une prime à la conversion pour les véhicules anciens sont prévues pour accompagner les plus fragiles dans la transition écologique.

Tableau : principales mesures nouvelles en prélèvements obligatoires à fin 2018

<div><div align="center"><table border="1"><tr><th/><th>
2018</th></tr><tr><td align="left" valign="middle">
Dégrèvement de la taxe d'habitation pour 80 % des ménages</td><td align="center" valign="middle">
- 3,0</td></tr><tr><td align="left" valign="middle">
Création de l'Impôt sur la Fortune Immobilière (IFI)</td><td align="center" valign="middle">
- 3,2</td></tr><tr><td align="left" valign="middle">
Mise en place d'un prélèvement forfaitaire unique</td><td align="center" valign="middle">
- 1,3</td></tr><tr><td align="left" valign="middle">
Baisse du taux d'IS de 33 % à 25 %</td><td align="center" valign="middle">
- 1,2</td></tr><tr><td align="left" valign="middle">
Suppression de la 4e tranche de la taxe sur les salaires</td><td align="center" valign="middle">
- 0,1</td></tr><tr><td align="left" valign="middle">
Hausse de la fiscalité énergétique</td><td align="center" valign="middle">
3,7</td></tr><tr><td align="left" valign="middle">
Fiscalité du tabac</td><td align="center" valign="middle">
0,5</td></tr><tr><td align="left" valign="middle">
CICE - Montée en charge et hausse de taux de 6 à 7 % en 2018</td><td align="center" valign="middle">
- 4,0</td></tr><tr><td align="left" valign="middle">
Crédit d'impôt sur la taxe sur les salaires</td><td align="center" valign="middle">
- 0,6</td></tr><tr><td align="left" valign="middle">
Elargissement du crédit d'impôt pour l'emploi de personnes à domicile</td><td align="center" valign="middle">
- 1,0</td></tr><tr><td align="left" valign="middle">
Total</td><td align="center" valign="middle">
- 10,3</td></tr><tr><td align="left" colspan="2" valign="middle">
Note : ce tableau n'intègre pas la mesure nouvelle de gain de pouvoir d'achat pour les actifs liée à la baisse des charges en basculement de la CSG dont la première étape aura lieu le 1er janvier et dont l'effet plein interviendra à compter d'octobre 2018.</td></tr></table></div></div>
C. - Une baisse de plus de 3 points de la dépense publique à horizon 2022

La trajectoire de la loi de programmation 2018-2022 prévoit une baisse du poids de la dépense publique hors crédits d'impôts dans le PIB de plus de 3 points à horizon 2022.
La dépense publique devrait ralentir lors des cinq prochaines années : sur la période 2018-2022, sa croissance en volume sera en moyenne de + 0,4 %, contre + 1,3 % sur les 10 dernières années. Cette croissance contenue sera rendue possible grâce aux importants efforts d'économies que l'ensemble des administrations publiques fourniront. Le rythme des dépenses s'infléchira au fur et à mesure de la mise en œuvre des économies structurelles identifiées par le processus « Action publique 2022 » (cf. infra).
Ainsi, sur le périmètre de la norme pilotable, l'évolution de la dépense de l'Etat en 2018 s'élèvera à + 1,0 % en volume, puis à - 0,5 % en volume en 2019, soit + 1,6 Md€ par rapport à 2018. Sur la période 2020-2022, alors que la charge de la dette augmentera de l'ordre de 0,1 point de PIB par an, le taux d'évolution en volume de la dépense sous norme pilotable sera de - 1 % par an et reposera notamment sur les réformes documentées dans le cadre du processus « Action Publique 2022 ».
De même, les collectivités locales maîtriseront leurs dépenses de fonctionnement dans le cadre du nouveau pacte financier avec l'Etat : en comptabilité nationale, les dépenses finales de fonctionnement ne progresseront que de 1,2 % en valeur en moyenne sur le quinquennat, en cohérence avec l'objectif fixé dans la loi de programmation. Ceci correspond à un effort de 13 Md€ par rapport à une trajectoire spontanée de dépenses.
Enfin, les dépenses sociales seront maitrisées : en dépit d'une reprise de l'inflation, l'ONDAM sera maintenu à 2,3 % sur la période 2018-2020 (cf. infra).

D. - A l'horizon du quinquennat, l'effort structurel portera sur la dépense et permettra la diminution du ratio de dette publique

Les tableaux suivants sont exprimés en comptabilité nationale et non en comptabilité budgétaire ou en comptabilité générale comme les objectifs d'économie ci-dessus. Ces différentes comptabilités peuvent ainsi présenter des écarts significatifs.

<div><div align="center"><table border="1"><tr><th/><th>
2016</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Solde public (en point de PIB)</td><td align="center" valign="middle">
- 3,4</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 2,8</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 1,5</td><td align="center" valign="middle">
- 0,9</td><td align="center" valign="middle">
- 0,3</td></tr><tr><td align="left" valign="middle">
Solde structurel (en points de PIB potentiel)</td><td align="center" valign="middle">
- 2,5</td><td align="center" valign="middle">
- 2,2</td><td align="center" valign="middle">
- 2,1</td><td align="center" valign="middle">
- 1,9</td><td align="center" valign="middle">
- 1,6</td><td align="center" valign="middle">
- 1,2</td><td align="center" valign="middle">
- 0,8</td></tr><tr><td align="left" valign="middle">
Ajustement structurel</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,4</td></tr></table></div></div>
Tableau : dépenses et recettes (exprimés suivant les conventions de la comptabilité nationale)

<div><div align="center"><table border="1"><tr><th/><th>
2016</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Ratio de dépenses publiques (*)</td><td align="center" valign="middle">
55,0</td><td align="center" valign="middle">
54,7</td><td align="center" valign="middle">
54,0</td><td align="center" valign="middle">
53,4</td><td align="center" valign="middle">
52,6</td><td align="center" valign="middle">
51,9</td><td align="center" valign="middle">
51,1</td></tr><tr><td align="left" valign="middle">
Ratio de prélèvements obligatoires</td><td align="center" valign="middle">
44,4</td><td align="center" valign="middle">
44,7</td><td align="center" valign="middle">
44,3</td><td align="center" valign="middle">
43,4</td><td align="center" valign="middle">
43,7</td><td align="center" valign="middle">
43,7</td><td align="center" valign="middle">
43,7</td></tr><tr><td align="left" valign="middle">
Ratio de recettes hors prélèvements obligatoires</td><td align="center" valign="middle">
7,5</td><td align="center" valign="middle">
7,3</td><td align="center" valign="middle">
7,2</td><td align="center" valign="middle">
7,2</td><td align="center" valign="middle">
7,2</td><td align="center" valign="middle">
7,1</td><td align="center" valign="middle">
7,1</td></tr><tr><td align="left" valign="middle">
Clé de crédits d'impôts</td><td align="center" valign="middle">
- 0,2</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,1</td></tr><tr><td align="left" colspan="8" valign="middle">
Note : (*) hors crédits d'impôts.</td></tr></table></div></div>
Tableau : croissance de la dépense publique hors crédits d'impôt

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Taux de croissance en valeur</td><td align="center" valign="middle">
1,9</td><td align="center" valign="middle">
1,6</td><td align="center" valign="middle">
1,8</td><td align="center" valign="middle">
1,7</td><td align="center" valign="middle">
2,0</td><td align="center" valign="middle">
1,9</td></tr><tr><td align="left" valign="middle">
Taux de croissance en volume</td><td align="center" valign="middle">
0,9</td><td align="center" valign="middle">
0,6</td><td align="center" valign="middle">
0,7</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,1</td></tr><tr><td align="left" valign="middle">
Inflation hors tabac</td><td align="center" valign="middle">
1,0</td><td align="center" valign="middle">
1,0</td><td align="center" valign="middle">
1,1</td><td align="center" valign="middle">
1,4</td><td align="center" valign="middle">
1,75</td><td align="center" valign="middle">
1,75</td></tr></table></div></div>
Tableau : dépense publique par sous-secteur, hors transferts, hors crédits d'impôts

<div><div align="center"><table border="1"><tr><th>
Croissance en volume</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
APU</td><td align="center" valign="middle">
0,9</td><td align="center" valign="middle">
0,6</td><td align="center" valign="middle">
0,7</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,1</td></tr><tr><td align="left" valign="middle">
APUC</td><td align="center" valign="middle">
1,0**</td><td align="center" valign="middle">
0,3**</td><td align="center" valign="middle">
0,8</td><td align="center" valign="middle">
1,2</td><td align="center" valign="middle">
0,7</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="left" valign="middle">
APUL</td><td align="center" valign="middle">
1,7</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,9</td><td align="center" valign="middle">
- 0,4</td><td align="center" valign="middle">
- 1,6</td><td align="center" valign="middle">
- 0,6</td></tr><tr><td align="left" valign="middle">
ASSO</td><td align="center" valign="middle">
0,6**</td><td align="center" valign="middle">
0,9**</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,6</td><td align="center" valign="middle">
0,4</td></tr><tr><td align="left" colspan="7" valign="middle">
Note : * Dépense en volume hors crédits d'impôts et hors transferts entre sous-secteurs des administrations publiques, exprimée à champ courant sauf mention contraire (**)
** Mesures retraitées :
En 2017, la budgétisation du financement des ESAT augmente la dépense des ASSO et diminue la dépense des APUC (+ 1,5 Md€).
En 2018, les prestations auparavant versées par le fonds social chômage, classé en APUC, sont transférées vers Pôle Emploi en ASSO (2,5 Md€).</td></tr></table></div></div>
1. L'effort structurel portera exclusivement sur la dépense publique

L'écart de production persistant en 2017 serait résorbé à l'horizon 2020, grâce à une croissance effective du PIB supérieure à la croissance potentielle. Sur le quinquennat, l'écart de production, encore très négatif en 2017 (- 1,1 % de PIB potentiel) deviendra positif en 2020 et s'établira en 2022 à + 1,1 % de PIB potentiel. Ainsi, la variation conjoncturelle du solde public contribuera de manière significative au redressement des finances publiques entre 2017 et 2022.
Cependant, la stratégie de redressement des finances publiques ira bien au-delà de l'effet mécanique de la résorption des effets de la crise passée : la France doit tirer profit de l'amélioration de la conjoncture économique pour engager un ajustement pérenne de ses finances publiques et en recomposer la structure par des choix stratégiques. Ainsi, le solde structurel, qui s'établit à - 2,2 % de PIB potentiel en 2017, se redressera à - 0,8 % en 2022. Cela permettra d'atteindre l'OMT, fixé à - 0,4 % de PIB potentiel, en 2023. A partir de 2019, l'ajustement structurel sera en moyenne de 0,3 point de PIB potentiel par an. Au total, il sera de 1,4 point de PIB potentiel entre 2017 et 2022.
Cet ajustement sera entièrement porté par un effort structurel en dépense : ce dernier sera de 2,4 points de PIB potentiel sur la période, grâce à une progression des dépenses publiques en volume nettement inférieure à la croissance potentielle de l'économie française. A l'inverse, les mesures nouvelles en prélèvement obligatoires visant à la simplification, la recomposition et la réorientation de nos recettes fiscales, pèseront sur l'ajustement structurel pour près de 1 point de PIB potentiel sur la période. Cette stratégie permettra de libérer l'activité, le pouvoir d'achat, et de stimuler l'investissement productif, risqué et innovant. Enfin, plus spécifiquement, compte tenu du traitement en comptabilité nationale du CICE comme une dépense, la disparition de ce crédit d'impôt en 2019 pour être remplacé par une baisse pérenne des cotisations sociales jouera favorablement sur l'ajustement structurel entre 2017 et 2022 (+ 0,3 point de PIB potentiel) : la créance en comptabilité nationale sera quasiment éteinte dès 2020, alors qu'un coût budgétaire persistera tout au long des restitutions. Au total, l'effort structurel, c'est-à-dire l'effet des mesures discrétionnaires, permettra d'améliorer le solde structurel de 1,8 point de PIB potentiel sur la période considérée.
Enfin, les mesures exceptionnelles et temporaires prévisibles dans le cadre du présent rapport perturberont la chronique de solde public de manière transitoire. En particulier, la transformation du CICE en allègement pérenne de cotisations sociales patronales entraînera en 2019 une double dépense qui pèsera lourdement (20,6 Md€) sur le déficit nominal.

Tableau : mesures exceptionnelles et temporaires - Hypothèses retenues dans la programmation

<div><div align="center"><table border="1"><tr><th>
(écart au compte central, en Md€)</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Mesures en recettes</td><td align="center" valign="middle">
- 1,5</td><td align="center" valign="middle">
- 4,9</td><td align="center" valign="middle">
- 22,5</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
dont :</td><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/></tr><tr><td align="left" valign="middle">
Contentieux OPCVM</td><td align="center" valign="middle">
- 0,8</td><td align="center" valign="middle">
- 0,8</td><td align="center" valign="middle">
- 0,7</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
De Ruyter</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
Stéria</td><td align="center" valign="middle">
- 0,5</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
3 % dividendes</td><td align="center" valign="middle">
- 4,5</td><td align="center" valign="middle">
- 4,5</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
Surtaxe d'IS</td><td align="center" valign="middle">
4,7</td><td align="center" valign="middle">
0,6</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
CVAE</td><td align="center" valign="middle">
- 0,3</td><td align="center" valign="middle">
- 0,2</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
Double coût bascule CITE</td><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="center" valign="middle">
- 1,1</td><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/></tr><tr><td align="left" valign="middle">
Double coût bascule CICE</td><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="center" valign="middle">
- 20,6</td><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/></tr><tr><td align="left" valign="middle">
Mesures en dépense</td><td align="center" valign="middle">
0,7</td><td align="center" valign="middle">
0,7</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
dont :</td><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/></tr><tr><td align="left" valign="middle">
Intérêts des contentieux</td><td align="center" valign="middle">
0,7</td><td align="center" valign="middle">
0,7</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr></table></div></div>
Tableau : variation du solde structurel des administrations publiques

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Variation du solde structurel (ajustement structurel)</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,4</td></tr><tr><td align="left" valign="middle">
Effort structurel</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,5</td></tr><tr><td align="left" valign="middle">
Mesures nouvelles en recettes (net des CI) et hors one-offs</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,3</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,5</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,1</td></tr><tr><td align="left" valign="middle">
Effort en dépense</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,6</td></tr><tr><td align="left" valign="middle">
Clé en crédits d'impôt</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
- 0,2</td></tr><tr><td align="left" valign="middle">
Composante non discrétionnaire</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td></tr></table></div></div>
Tableau : élasticité des prélèvements obligatoires

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Elasticité des prélèvements obligatoires (hors UE)</td><td align="center" valign="middle">
1,4</td><td align="center" valign="middle">
1,0</td><td align="center" valign="middle">
1,0</td><td align="center" valign="middle">
1,0</td><td align="center" valign="middle">
1,0</td><td align="center" valign="middle">
1,0</td></tr></table></div></div>
Tableau : décomposition structurelle par sous-secteur

<div><div align="center"><table border="1"><tr><th colspan="2">
En % PIB</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" rowspan="4" valign="middle">
APU</td><td align="left" valign="middle">
Solde effectif</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 2,8</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 1,5</td><td align="center" valign="middle">
- 0,9</td><td align="center" valign="middle">
- 0,3</td></tr><tr><td align="left" valign="middle">
Solde conjoncturel</td><td align="center" valign="middle">
- 0,6</td><td align="center" valign="middle">
- 0,4</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,6</td></tr><tr><td align="left" valign="middle">
Solde structurel</td><td align="center" valign="middle">
- 2,2</td><td align="center" valign="middle">
- 2,1</td><td align="center" valign="middle">
- 1,9</td><td align="center" valign="middle">
- 1,6</td><td align="center" valign="middle">
- 1,2</td><td align="center" valign="middle">
- 0,8</td></tr><tr><td align="left" valign="middle">
Solde des one-offs</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,2</td><td align="center" valign="middle">
- 0,9</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" rowspan="4" valign="middle">
APUC</td><td align="left" valign="middle">
Solde effectif</td><td align="center" valign="middle">
- 3,2</td><td align="center" valign="middle">
- 3,4</td><td align="center" valign="middle">
- 3,9</td><td align="center" valign="middle">
- 2,6</td><td align="center" valign="middle">
- 2,3</td><td align="center" valign="middle">
- 1,8</td></tr><tr><td align="left" valign="middle">
Solde conjoncturel</td><td align="center" valign="middle">
- 0,3</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="left" valign="middle">
Solde structurel</td><td align="center" valign="middle">
- 2,8</td><td align="center" valign="middle">
- 3,0</td><td align="center" valign="middle">
- 3,7</td><td align="center" valign="middle">
- 2,6</td><td align="center" valign="middle">
- 2,4</td><td align="center" valign="middle">
- 2,0</td></tr><tr><td align="left" valign="middle">
Solde des one-offs</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,2</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" rowspan="4" valign="middle">
APUL</td><td align="left" valign="middle">
Solde effectif</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,7</td></tr><tr><td align="left" valign="middle">
Solde conjoncturel</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,1</td></tr><tr><td align="left" valign="middle">
Solde structurel</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,7</td></tr><tr><td align="left" valign="middle">
Solde des one-offs</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" rowspan="4" valign="middle">
ASSO</td><td align="left" valign="middle">
Solde effectif</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,8</td><td align="center" valign="middle">
0,8</td><td align="center" valign="middle">
0,8</td><td align="center" valign="middle">
0,8</td></tr><tr><td align="left" valign="middle">
Solde conjoncturel</td><td align="center" valign="middle">
- 0,3</td><td align="center" valign="middle">
- 0,2</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,3</td></tr><tr><td align="left" valign="middle">
Solde structurel</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,7</td><td align="center" valign="middle">
1,7</td><td align="center" valign="middle">
0,8</td><td align="center" valign="middle">
0,6</td><td align="center" valign="middle">
0,5</td></tr><tr><td align="left" valign="middle">
Solde des one-offs</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
- 0,9</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr></table></div></div>
Tableau : effort structurel par sous-secteur

<div><div align="center"><table border="1"><tr><th colspan="2">
En % PIB</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" rowspan="5" valign="middle">
APU</td><td align="left" valign="middle">
Variation du solde structurel</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,4</td></tr><tr><td align="left" valign="middle">
dont effort structurel</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,5</td></tr><tr><td align="left" valign="middle">
Effort en recettes</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,3</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,5</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,1</td></tr><tr><td align="left" valign="middle">
Effort en dépense</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,6</td></tr><tr><td align="left" valign="middle">
Clé de crédits d'impôts</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
- 0,2</td></tr><tr><td align="left" rowspan="5" valign="middle">
APUC</td><td align="left" valign="middle">
Variation du solde structurel</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
- 0,2</td><td align="center" valign="middle">
- 0,8</td><td align="center" valign="middle">
1,1</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,4</td></tr><tr><td align="left" valign="middle">
dont effort structurel</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
- 0,2</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,9</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td></tr><tr><td align="left" valign="middle">
Effort en recettes</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,4</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
Effort en dépense</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="left" valign="middle">
Clé de crédits d'impôts</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
- 0,2</td></tr><tr><td align="left" rowspan="5" valign="middle">
APUL</td><td align="left" valign="middle">
Variation du solde structurel</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="left" valign="middle">
dont effort structurel</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="left" valign="middle">
Effort en recettes</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
Effort en dépense</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="left" valign="middle">
Clé de crédits d'impôts</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" rowspan="5" valign="middle">
ASSO</td><td align="left" valign="middle">
Variation du solde structurel</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
1,0</td><td align="center" valign="middle">
- 1,0</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td></tr><tr><td align="left" valign="middle">
dont effort structurel</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
- 0,6</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="left" valign="middle">
Effort en recettes</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
- 0,2</td><td align="center" valign="middle">
- 0,9</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr><tr><td align="left" valign="middle">
Effort en dépense</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="left" valign="middle">
Clé de crédits d'impôts</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
0,0</td></tr></table></div></div>
2. Le ratio de dette des administrations publiques sera réduit de 5 points pendant le quinquennat

La trajectoire de réduction des dépenses publiques de plus de 3 points de PIB et l'augmentation de la croissance, stimulée par les réformes prévues pendant le quinquennat, permettront à horizon 2022 de réduire le ratio de dette publique de 5 points. Cette baisse du ratio d'endettement permettra de renforcer la résilience et de dégager des marges de manœuvre budgétaires, en cas de nouvelle crise, améliorant ainsi la soutenabilité des finances publiques.
Le ratio d'endettement devrait ainsi diminuer à partir de 2020 après avoir atteint un pic en 2019. Cette augmentation en 2019 serait due au « double coût » exceptionnel lié à la transformation du CICE en baisse pérenne de cotisations. A partir de 2020, la baisse du déficit liée à la poursuite des efforts de consolidation et à la disparition de ce surcoût temporaire permettrait de commencer à faire décroître le ratio de dette. Des flux de créance viendraient cependant compenser en partie les effets de la baisse du déficit et de la croissance sur le ratio de dette. L'effet principal serait lié à l'extinction du CICE entre 2020 et 2022. En effet, à partir de 2019 aucun nouveau droit au CICE ne serait acquis mais il resterait un stock de créances fiscales acquises les années précédentes à restituer aux entreprises. Cet effet de trésorerie, neutre sur le solde public à partir de 2019, a néanmoins un impact sur le ratio de dette. Il s'estompe au fur et à mesure que le stock de restitution diminue. Malgré ces flux de créances pesant sur la dette, le ratio dette sur PIB diminuerait ainsi de plus de 5 points entre fin 2019 et 2022.

Tableau : la trajectoire de dette publique des administrations publiques, et détail par sous-secteur

<div><div align="center"><table border="1"><tr><th>
(En points de PIB)</th><th>
2016</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Ratio d'endettement au sens de Maastricht</td><td align="center" valign="middle">
96,3</td><td align="center" valign="middle">
96,7</td><td align="center" valign="middle">
96,9</td><td align="center" valign="middle">
97,1</td><td align="center" valign="middle">
96,1</td><td align="center" valign="middle">
94,2</td><td align="center" valign="middle">
91,4</td></tr><tr><td align="left" valign="middle">
Contribution des administrations publiques centrales (APUC)</td><td align="center" valign="middle">
77,3</td><td align="center" valign="middle">
78,3</td><td align="center" valign="middle">
79,4</td><td align="center" valign="middle">
81,1</td><td align="center" valign="middle">
81,7</td><td align="center" valign="middle">
81,6</td><td align="center" valign="middle">
80,8</td></tr><tr><td align="left" valign="middle">
Contribution des administrations publiques locales (APUL)</td><td align="center" valign="middle">
9,0</td><td align="center" valign="middle">
8,7</td><td align="center" valign="middle">
8,4</td><td align="center" valign="middle">
8,1</td><td align="center" valign="middle">
7,5</td><td align="center" valign="middle">
6,7</td><td align="center" valign="middle">
5,8</td></tr><tr><td align="left" valign="middle">
Contribution des administrations de sécurité sociale (ASSO)</td><td align="center" valign="middle">
10,1</td><td align="center" valign="middle">
9,7</td><td align="center" valign="middle">
9,0</td><td align="center" valign="middle">
8,0</td><td align="center" valign="middle">
6,9</td><td align="center" valign="middle">
5,9</td><td align="center" valign="middle">
4,8</td></tr></table></div></div>
Tableau : l'écart au solde stabilisant, le flux de créances et la variation du ratio d'endettement

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Variation du ratio d'endettement (1 + 2)</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
- 1,1</td><td align="center" valign="middle">
- 1,9</td><td align="center" valign="middle">
- 2,8</td></tr><tr><td align="left" valign="middle">
Ecart au solde stabilisant (1) = (a - b)</td><td align="center" valign="middle">
0,6</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
- 1,6</td><td align="center" valign="middle">
v2,3</td><td align="center" valign="middle">
v3,0</td></tr><tr><td align="left" valign="middle">
Solde stabilisant la dette (a)</td><td align="center" valign="middle">
- 2,3</td><td align="center" valign="middle">
- 2,7</td><td align="center" valign="middle">
- 2,8</td><td align="center" valign="middle">
- 3,0</td><td align="center" valign="middle">
- 3,2</td><td align="center" valign="middle">
- 3,3</td></tr><tr><td align="left" valign="middle">
Pm. Croissance nominale</td><td align="center" valign="middle">
2,5</td><td align="center" valign="middle">
2,9</td><td align="center" valign="middle">
3,0</td><td align="center" valign="middle">
3,2</td><td align="center" valign="middle">
3,5</td><td align="center" valign="middle">
3,6</td></tr><tr><td align="left" valign="middle">
Solde public au sens de Maastricht (b)</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 2,8</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 1,5</td><td align="center" valign="middle">
- 0,9</td><td align="center" valign="middle">
- 0,3</td></tr><tr><td align="left" valign="middle">
Flux de créances (2)</td><td align="center" valign="middle">
- 0,2</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
0,2</td></tr></table></div></div>
Encadré - Coût de financement de l'Etat et hypothèses de taux d'intérêt

En 2017 la France continue de bénéficier de conditions de financement très favorables, grâce au maintien de la confiance des investisseurs et aux effets de la politique monétaire accommodante menée par la Banque centrale européenne (BCE). A mi-septembre 2017 le taux moyen à l'émission des titres à court terme (BTF) s'établit à - 0,60 % après - 0,53 % en 2016, celui des titres à moyen-long terme à 0,71 % après 0,37 % en 2016. Du fait de l'affermissement de la croissance en Europe et du retour graduel de l'inflation enclenché depuis mi 2016, la perspective d'un resserrement progressif de la politique monétaire de la BCE se confirme.
Le profil de taux sous-jacent à la prévision de la charge de la dette repose sur l'hypothèse d'un resserrement graduel de la politique monétaire européenne à partir de 2018, en cohérence avec le scénario macroéconomique global de consolidation de la croissance et de l'inflation. Il table sur une poursuite du redressement des taux de moyen-long terme au rythme moyen de 75 points de base par an. Le taux à dix ans s'établirait à 1,85 % fin 2018, puis 2,60 % fin 2019 et 3,75 % fin 2021.

<div><div align="center"><table border="1"><tr><th>
Niveaux en fin d'année (hypothèses)</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Taux courts (BTF 3 mois)
Taux longs (OAT à 10 ans)</td><td align="center" valign="middle">
- 0,50 %
1,10 %</td><td align="center" valign="middle">
- 0,10 %
1,85 %</td><td align="center" valign="middle">
0,70 %
2,60 %</td><td align="center" valign="middle">
1,50 %
3,25 %</td><td align="center" valign="middle">
2,00 %
3,75 %</td><td align="center" valign="middle">
2,50 %
4,00 %</td></tr></table></div></div>
Tableau : charge d'intérêts et solde primaire

<div><div align="center"><table border="1"><tr><th/><th>
2016</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Solde public</td><td align="center" valign="middle">
- 3,4</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 2,8</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 1,5</td><td align="center" valign="middle">
- 0,9</td><td align="center" valign="middle">
- 0,3</td></tr><tr><td align="left" valign="middle">
Charge d'intérêt</td><td align="center" valign="middle">
1,9</td><td align="center" valign="middle">
1,8</td><td align="center" valign="middle">
1,8</td><td align="center" valign="middle">
1,8</td><td align="center" valign="middle">
1,9</td><td align="center" valign="middle">
2,0</td><td align="center" valign="middle">
2,1</td></tr><tr><td align="left" valign="middle">
Solde primaire</td><td align="center" valign="middle">
- 1,5</td><td align="center" valign="middle">
- 1,1</td><td align="center" valign="middle">
- 1,0</td><td align="center" valign="middle">
- 1,2</td><td align="center" valign="middle">
0,4</td><td align="center" valign="middle">
1,1</td><td align="center" valign="middle">
1,8</td></tr></table></div></div>
E. - Une transformation de l'action et de la gestion publique
1. Action publique 2022

Lancé dès 2017, le processus « Action publique 2022 » permettra de dégager des économies structurelles à moyen terme. La trajectoire fixée par la présente loi prévoit une baisse de plus de trois points de PIB de la dépense publique. Le Gouvernement entend atteindre cet objectif, en examinant le périmètre de l'action publique d'un point de vue stratégique et en impliquant les usagers et les agents, afin d'améliorer l'efficience des politiques publiques : meilleure qualité de service pour les usagers, meilleur environnement de travail pour les agents, et meilleure utilisation des impôts des contribuables. Cela suppose de mettre en œuvre un processus ambitieux de réforme de l'action publique. S'appuyant sur une démarche de revue des missions et de la dépense publique dans une approche par politique publique, le programme « Action publique 2022 » portera sur la définition et la mise en œuvre de réformes structurelles qui se déploieront au cours du quinquennat.
Constatant que les exercices de réforme de l'Etat et des administrations publiques qui se succèdent depuis dix ans (« Révision générale des politiques », de 2007 à 2012 et « Modernisation de l'action publique » de 2012 à 2017), et plus généralement les approches par le rabot, n'ont pas été à la hauteur des enjeux de redressement des finances publiques et de modernisation de l'action publique, le Gouvernement souhaite lancer un processus de réforme d'un type nouveau, placé sous l'autorité du Premier ministre, avec l'appui du ministre de l'action et des comptes publics.
Il couvre l'ensemble des administrations publiques et de la dépense publique, dans la mesure où le champ de l'Etat ne représente qu'une part minoritaire de la dépense publique et dans la mesure où les usagers du service public portent une appréciation sur sa qualité sans distinguer la collectivité qui les porte. Toutefois, afin de garantir une responsabilisation accrue, chaque ministère sera chef de file des politiques publique qui le concernent.
Un Comité Action Publique 2022 (CAP22), composé de personnalités qualifiées françaises ou étrangères issues de la société civile, de hauts fonctionnaires et d'élus locaux, sera chargé d'identifier des réformes structurelles et des économies significatives et durables, sur l'ensemble du champ des administrations publiques, en faisant émerger des idées et des méthodes nouvelles. Pour ce faire, plusieurs chantiers viendront alimenter les travaux du comité. Les propositions des ministères seront examinées dans le cadre de travaux itératifs. Cinq chantiers transversaux seront conduits en parallèle sur les thématiques suivantes : la simplification administrative, la transformation numérique, la rénovation du cadre des ressources humaines, l'organisation territoriale des services publics et la modernisation de la gestion budgétaire et comptable. En parallèle, un grand forum de l'action publique permettra d'associer les usagers et les agents à la rénovation de l'action publique.
Les conclusions du Comité Action Publique 2022, dévoilées au premier trimestre 2018, feront l'objet d'arbitrages sur la base desquels des plans de transformation ministériels seront élaborés et mis en place.

2. La maîtrise des dépenses de l'Etat

Le budget pluriannuel présenté à l'occasion de la présente LPFP 2018-2022 reposera sur un double système de norme : une norme recentrée sur les dépenses pilotables de l'Etat et un objectif de dépenses totales de l'Etat (ODETE) telles que définies à l'article 9 de la loi.
a) Un principe directeur de sincérité de la budgétisation initiale :
Les crédits nécessaires à la couverture des sous-budgétisations identifiés par la Cour des comptes dans son rapport d'audit seront en effet ouverts dans le projet de loi de finances pour 2018, permettant ainsi une plus grande transparence vis-à-vis de la représentation nationale. Un tel effort de remise à niveau doit permettre une plus grande responsabilisation des gestionnaires qui devront donc gérer aléas ou priorités nouvelles au sein de leur plafond limitatif de crédits.
Le principe d'auto-assurance est un corollaire indispensable de la visibilité donnée sur les enveloppes triennales. Il est le pendant de la sincérisation des budgets opérée à l'occasion du PLF 2018. En construction budgétaire (pour les budgets à venir au-delà de celui de 2018) comme en gestion, ce principe implique que les aléas ou les priorités nouvelles affectant les dépenses d'une mission soient gérés dans la limite du plafond de ses crédits, soit par redéploiement de dépenses discrétionnaires, soit par la réalisation d'économies complémentaires. Ces redéploiements ou économies doivent être mis en œuvre prioritairement au sein du programme qui supporte les aléas ou les priorités nouvelles. A défaut, ils doivent être réalisés entre les programmes de la même mission.

Sous-budgétisations sous-jacentes à la LFI 2017 identifiées par la Cour des comptes

<div><div align="center"><table border="1"><tr><th>
Mission</th><th>
Sous-budgétisation</th><th>
Montant (Md€)</th></tr><tr><td align="center" valign="middle">
Agriculture, alimentation, forêt et affaires rurales</td><td align="center" valign="middle">
Refus d'apurements communautaires</td><td align="center" valign="middle">
0,4</td></tr><tr><td align="center" valign="middle">
Agriculture</td><td align="center" valign="middle">
Crises : Influenza aviaire</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="center" valign="middle">
Travail et emploi</td><td align="center" valign="middle">
Prime à l'embauche</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="center" valign="middle">
Travail et emploi</td><td align="center" valign="middle">
Rémunération de fin de formation</td><td align="center" valign="middle">
0,1</td></tr><tr><td align="center" valign="middle">
Travail et emploi</td><td align="center" valign="middle">
Plan de formation</td><td align="center" valign="middle">
0,4</td></tr><tr><td align="center" valign="middle">
Travail et emploi</td><td align="center" valign="middle">
Contrats aidés</td><td align="center" valign="middle">
0,3</td></tr><tr><td align="center" valign="middle">
Défense</td><td align="center" valign="middle">
Opérations extérieures, opérations intérieures et masse salariale</td><td align="center" valign="middle">
0,7</td></tr><tr><td align="center" valign="middle">
Solidarités et santé</td><td align="center" valign="middle">
Allocation aux adultes handicapés (AAH) et Prime d'activité</td><td align="center" valign="middle">
0,7</td></tr><tr><td align="center" valign="middle">
Enseignement scolaire</td><td align="center" valign="middle">
Masse salariale de l'éducation nationale</td><td align="center" valign="middle">
0,4</td></tr><tr><td align="center" valign="middle">
Immigration, asile et intégration</td><td align="center" valign="middle">
Allocation pour demandeurs d'asile (ADA)</td><td align="center" valign="middle">
0,2</td></tr><tr><td align="center" valign="middle">
Cohésion des territoires</td><td align="center" valign="middle">
Hébergement d'urgence</td><td align="center" valign="middle">
0,1</td></tr><tr><td align="center" valign="middle">
Cohésion des territoires</td><td align="center" valign="middle">
Aides personnalisées au logement</td><td align="center" valign="middle">
0,1</td></tr><tr><td align="center" valign="middle">
Autres</td><td align="center" valign="middle">
Service civique, Aide médicale d'Etat, contribution aux organismes scientifiques internationaux, etc.</td><td align="center" valign="middle">
0,5</td></tr><tr><td align="left" valign="middle"/><td align="center" valign="middle">
Total (Md€)</td><td align="center" valign="middle">
4,2</td></tr><tr><td align="left" colspan="3" valign="middle">
Source : Cour des comptes, La situation et les perspectives des finances publiques (juin 2017).</td></tr></table></div></div>
La mise en œuvre du principe d'auto-assurance permet ainsi de limiter aux seules situations exceptionnelles les ajustements susceptibles d'affecter en cours d'année les plafonds définis par mission ou leur révision dans le cadre des projets de lois de finances, selon les modalités précisées ci-après.
b) Réserve de budgétisation et de précaution :
L'incapacité de prévoir la totalité des événements susceptibles d'intervenir et la nécessité de pouvoir initier des actions nouvelles en cours de programmation justifient l'existence de marges de manœuvre tant pour la budgétisation au sein d'une période pluriannuelle, qu'en gestion.
Concernant la budgétisation, la programmation triennale prévoit une réserve centrale de budgétisation d'un montant de 0,7 Md€ en 2020. La mise en place d'une telle réserve, objectif qui n'avait pu être atteint depuis le triennal 2009-2011, doit permettre de répondre aux situations nouvelles qui ne pouvaient être anticipées au stade de la programmation pluriannuelle. En priorité, la réserve de budgétisation permettra en 2020, de rehausser le cas échéant les plafonds de mission soumis à des dépenses plus dynamiques que prévues ou imprévisibles, qui ne pourraient être absorbées au sein de la mission après application du principe d' « auto-assurance » décrit supra.
Concernant l'exécution, le Gouvernement s'engage à revenir à une pratique plus raisonnée des outils de régulation et de redéploiement en gestion. Pour cela, l'accent a été mis sur la sincérité de la budgétisation initiale (cf. supra). Les enveloppes de couverture des risques en gestion sont par ailleurs majorées : la dotation pour dépenses accidentelles et imprévisibles (DDAI) est reconstituée à hauteur de 124 M€ afin de permettre de faire face à des événements imprévisibles nécessitant une intervention rapide de l'Etat (catastrophe naturelle par exemple), la provision pour opérations extérieures de la défense sera progressivement rebasée, avec une première marche de + 200 M€ en 2018, et pour la première fois une provision pour risques est inscrite au sein du budget de l'agriculture, à hauteur de 300 M€.
Ces mesures, accompagnées de la réaffirmation du principe d'auto-assurance, permettent d'abaisser fortement le niveau de la réserve de précaution - dont le principe est posé au 4° bis de l'article 51 de la LOLF et qui vise à bloquer une partie des crédits en début de gestion - en le ramenant de 8 % à 3 % des crédits initiaux.
c) Doctrine d'affectation de taxes :
A l'inverse des dotations versées par le budget général de l'Etat, l'affectation directe d'impositions de toute nature fait échapper ce financement public au contrôle annuel du Parlement dans le cadre de l'examen du projet de loi de finances. L'affectation constitue par ailleurs une dérogation au principe d'universalité budgétaire. Enfin, la plupart des taxes affectées connaissent une évolution dynamique, ce qui, en l'absence de mécanisme de limitation, conduit à une progression de la dépense moins bien contrôlée - et donc moins en adéquation avec les besoins des politiques visées - pour les organismes concernés que pour les dépenses de l'Etat ou des organismes financés sur subvention budgétaire incluse dans la norme de dépenses pilotables de l'Etat.
Le recours à l'affectation d'imposition de toutes natures à des tiers autres que les organismes de sécurité sociale, les collectivités territoriales et les établissements publics de coopération intercommunale fait l'objet d'un encadrement précis à l'article 15 de la présente LPFP qui exclut expressément l'affectation de taxes, à trois exceptions près :
la première exception est celle des ressources présentant une logique de « quasi-redevance » ;
la seconde exception concerne les prélèvements finançant des actions de mutualisation ou de solidarités sectorielles au sein d'un secteur d'activité ;
enfin, la troisième exception est celle des fonds d'assurance ou d'indemnisation et présentant une logique de mutualisation du risque.
Le même article indique que l'ensemble des taxes affectées doivent faire l'objet d'un plafonnement. Les éventuelles affectations dérogeant à ce principe de plafonnement sont justifiées dans l'annexe « Voies et moyens » du projet de loi de finances de l'année.

3. L'effort des collectivités locales fera l'objet d'une contractualisation avec l'Etat

Sur la durée du quinquennat, la maîtrise des dépenses publiques nécessitera la participation de chacun des sous-secteurs des administrations publiques. Les collectivités territoriales seront associées à cet effort avec une réduction de leurs dépenses à hauteur de 13 Md€ par rapport au tendanciel.
Cet objectif fera l'objet d'un contrat de mandature entre l'Etat et les collectivités locales qui permettra de définir leurs engagements réciproques. Un pacte financier, dont le contenu sera concerté dans le cadre de la conférence nationale des territoires, précisera à cet effet la trajectoire pluriannuelle de dépenses et de désendettement associée, les modalités de mise en œuvre des procédures visant à vérifier le respect des engagements ainsi que le mécanisme de correction applicable dans le cas d'une déviation des dépenses et de la trajectoire de désendettement à la trajectoire prévue.
La loi de programmation des finances publiques s'inscrit dans cette démarche en précisant les principes de la contractualisation avec les collectivités tant dans le périmètre retenu que dans le contenu des contrats et les possibilités de modulation offertes ou encore dans les mécanismes de reprise financière prévus.
L'objectif d'évolution de la dépense locale (ODEDEL) sera à nouveau utilisé sur la période de programmation. Il constituera un outil qui permettra de vérifier, en concertation avec les collectivités, le respect de leur trajectoire de dépenses de fonctionnement.

4. Les dépenses fiscales et les niches sociales feront l'objet d'un suivi renforcé

La LPFP 2018-2022 prévoit un mécanisme d'encadrement de la part des dépenses fiscales dans l'ensemble des recettes fiscales du budget général, combiné à un mécanisme similaire de mesure de la part des niches sociales dans les recettes sociales, afin que dépenses fiscales et niches sociales ne puissent excéder un plafond exprimé en pourcentage des recettes totales inscrit en LPFP.
En parallèle, la LPFP 2018-2022 pose le principe d'une limitation dans le temps des dépenses fiscales et des niches sociales : tout nouveau texte instituant une dépense fiscale ou sociale doit prévoir un délai limité d'application maximal de quatre ans pour la niche fiscale et trois ans pour la niche sociale ainsi créée. Une telle disposition permet de fixer une échéance d'évaluation, à l'approche de la date d'extinction du dispositif, afin de justifier sa pertinence avant d'en proposer la reconduction au Parlement.

5. Le Grand plan d'investissement sera lancé dès 2018

a) Grand plan d'investissement :
Initiative majeure des cinq prochaines années, le grand plan d'investissement (GPI) a pour ambition d'accélérer l'émergence d'un nouveau modèle de croissance en poursuivant trois objectifs : augmenter son potentiel de croissance et d'emplois, privilégier la dépense publique favorable à l'investissement et l'innovation et accélérer la transition écologique. Ce plan, constitué de dépenses non pérennes, améliorera nettement les performances économiques et sociales du pays. Il contribuera à déployer le plein potentiel des réformes structurelles qui seront mises en œuvre en parallèle, et accompagnera les réformes budgétaires.
Les investissements prévus par le plan couvriront un champ très large de dépenses dépassant largement l'investissement public au strict sens comptable (formation brute de capital fixe), en englobant toute mobilisation temporaire de ressources ayant un effet à long terme, par exemple les actions de formation ou subventions destinées à orienter le comportement des acteurs dans la transition énergétique). Le plan sera ainsi centré autour de trois priorités : augmenter le potentiel de production, accélérer la transition énergétique et réduire structurellement la dépense publique en modernisant l'action publique. Il interviendra sur sept axes de politiques publiques : la formation et les compétences, la transition écologique et énergétique, la santé, l'agriculture, la modernisation des administrations publiques notamment grâce à la numérisation, les transports et équipements collectifs locaux, et l'enseignement supérieur, la recherche et l'innovation. Afin d'engager la transformation de notre modèle de croissance dès 2018, le Grand plan d'investissement montera en charge à partir de l'an prochain.
Conformément au principe de responsabilisation voulu par le Gouvernement, afin de donner aux ministres une pleine visibilité et responsabilité sur l'ensemble de leurs moyens, et contrairement aux ressources des programmes successifs d'investissements d'avenir, les crédits du GPI ne constitueront pas un budget distinct des programmes ministériels. Il s'agit donc de dépenses d'avenir et d'investissements temporaires qui seront retracées par les ministères eux-mêmes au sein de leurs budgets. En contrepartie, un suivi transversal sera effectué par une structure légère centralisée afin d'évaluer l'atteinte des objectifs et soutenir les ministères dans la mise en œuvre de ces actions de transformation.
Le plan sera composé de crédits à impact maastrichtien hors investissements d'avenir à hauteur de 36 Md€ sur le budget de l'Etat, de ses opérateurs, et de l'assurance maladie (ces dernières dépenses seront très largement financés au sein de l'objectif national d'assurance maladie - Ondam), de crédits du troisième PIA pour 10 Md€ (dont 6 Md€ de dépenses maastrichtiennes) et d'instruments financiers innovants non maastrichtiens (mobilisant des fonds propres et des prêts, notamment de la Caisse des dépôts et Consignations) pour 11 Md€ environ.
b) Gouvernance des investissements :
En valeur absolue, et depuis de nombreuses années, la France est le premier investisseur de l'Union européenne, avec 78,6 Md€ d'investissements publics au sens comptable (3) en 2015. La France ne souffre donc pas d'un problème de niveau d'investissement public. En revanche, l'utilisation optimale des deniers publics requiert d'investir mieux et de sélectionner les projets les plus utiles à la collectivité, c'est-à-dire ceux dont les gains pour la collectivité surpassent le plus les coûts. Ainsi, une décision d'investissement doit être éclairée par une comparaison objective de l'ensemble des coûts et des bénéfices socio-économiques du projet envisagé. Si la LPFP 2012-2017 a systématisé l'évaluation socio-économique des investissements, cette dernière reste variable selon les secteurs concernés et l'évaluation socioéconomique n'est d'une manière générale pas suffisamment prise en compte dans les décisions d'investissements.
Le Gouvernement a pour objectif d'améliorer cet état de fait, via i) la définition d'une méthodologie harmonisée, partagée et utilisée par tous les porteurs de projets et ii) une réforme du processus d'instruction et de décision des projets d'investissements. Un comité d'experts mis en place sous l'égide de France Stratégie devra préciser et harmoniser les règles de l'évaluation socio-économique pour l'ensemble des secteurs, avec notamment la publication d'un guide d'ici la fin de l'année. De plus le Gouvernement mettra en place un comité interministériel de sélection des investissements placé sous l'autorité du Premier ministre et qui sera chargé de valider ou non les étapes clefs de la vie d'un projet : approbation du principe du projet après notamment analyse socio-économique et analyse de soutenabilité budgétaire, validation du mode de réalisation et du plan de financement, autorisation d'engagement juridique et financier. Ces mesures contribueront à renforcer durablement la qualité de l'investissement public et la visibilité collective sur l'avancement des projets.

III. - L'effort sera équitablement réparti entre les sous-secteurs des administrations publiques
A. - La trajectoire de l'Etat

Sur la période de la programmation, la trajectoire de finances publiques de l'Etat connaîtrait l'évolution suivante :

Tableau : variation du solde structurel des administrations publiques

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Solde en comptabilité nationale (en Md€)</td><td align="center" valign="middle">
- 70,8</td><td align="center" valign="middle">
- 77,0</td><td align="center" valign="middle">
- 91,8</td><td align="center" valign="middle">
- 62,7</td><td align="center" valign="middle">
- 56,5</td><td align="center" valign="middle">
- 47,4</td></tr><tr><td align="left" valign="middle">
Solde en comptabilité nationale (en pt de PIB)</td><td align="center" valign="middle">
- 3,1</td><td align="center" valign="middle">
- 3,3</td><td align="center" valign="middle">
- 3,8</td><td align="center" valign="middle">
- 2,5</td><td align="center" valign="middle">
- 2,2</td><td align="center" valign="middle">
- 1,8</td></tr><tr><td align="left" valign="middle">
Dépenses (en pt de PIB)</td><td align="center" valign="middle">
21,3</td><td align="center" valign="middle">
21,0</td><td align="center" valign="middle">
20,7</td><td align="center" valign="middle">
19,7</td><td align="center" valign="middle">
19,4</td><td align="center" valign="middle">
19,0</td></tr><tr><td align="left" valign="middle">
Recettes (en pt de PIB)</td><td align="center" valign="middle">
18,2</td><td align="center" valign="middle">
17,8</td><td align="center" valign="middle">
16,9</td><td align="center" valign="middle">
17,2</td><td align="center" valign="middle">
17,2</td><td align="center" valign="middle">
17,3</td></tr></table></div></div>
1. La trajectoire des dépenses et des recettes de l'Etat

<div><div align="center"><table border="1"><tr><th/><th>
LFI 2017</th><th>
LFI 2017 format 2018</th><th>
PLF 2018</th><th>
2019</th><th>
2020</th></tr><tr><td align="left">
Crédits ministériels</td><td align="right">
234,6</td><td align="right">
236,0</td><td align="right">
241,3</td><td align="right">
242,6</td><td align="right">
247,5</td></tr><tr><td align="left">
Taxes affectées</td><td align="right">
9,6</td><td align="right">
9,3</td><td align="right">
9,1</td><td align="right">
8,9</td><td align="right">
9,0</td></tr><tr><td align="left">
Budgets annexes et comptes spéciaux pilotables</td><td align="right">
13,3</td><td align="right">
13,3</td><td align="right">
13,3</td><td align="right">
13,8</td><td align="right">
14,1</td></tr><tr><td align="left">
Retraitements des flux internes au budget de l'Etat</td><td align="right">
- 5,8</td><td align="right">
- 5,8</td><td align="right">
- 5,8</td><td align="right">
- 5,9</td><td align="right">
- 5,9</td></tr><tr><td align="left">
Economies complémentaires Action publique 2022</td><td align="left"/><td align="left"/><td align="left"/><td align="left"/><td align="right">
- 4,1</td></tr><tr><td align="left">
Norme de dépenses pilotables</td><td align="right">
251,7</td><td align="right">
252,8</td><td align="right">
257,9</td><td align="right">
259,5</td><td align="right">
260,5</td></tr><tr><td align="left">
Evolution annuelle en volume</td><td align="left"/><td align="left"/><td align="right">
1,0 %</td><td align="right">
- 0,5 %</td><td align="right">
- 1,0 %</td></tr><tr><td align="left">
Transferts aux collectivités territoriales</td><td align="right">
47,0</td><td align="right">
47,1</td><td align="right">
47,0</td><td align="right">
47,1</td><td align="right">
47,5</td></tr><tr><td align="left">
Dépenses du CAS Pensions (hors programme 743)</td><td align="right">
55,7</td><td align="right">
55,7</td><td align="right">
56,5</td><td align="right">
57,8</td><td align="right">
59,0</td></tr><tr><td align="left">
Autres dépenses de comptes d'affectation spéciale</td><td align="right">
1,5</td><td align="right">
1,5</td><td align="right">
1,8</td><td align="right">
2,0</td><td align="right">
1,0</td></tr><tr><td align="left">
Charge de la dette</td><td align="right">
41,5</td><td align="right">
41,5</td><td align="right">
41,2</td><td align="right">
41,9</td><td align="right">
44,7</td></tr><tr><td align="left">
Prélèvement sur recettes au profit de l'Union européenne</td><td align="right">
18,7</td><td align="right">
18,7</td><td align="right">
19,9</td><td align="right">
23,3</td><td align="right">
24,1</td></tr><tr><td align="left">
Investissements d'avenir</td><td align="right">
0,0</td><td align="right">
0,0</td><td align="right">
1,1</td><td align="right">
1,1</td><td align="right">
1,9</td></tr><tr><td align="left">
Objectif de dépenses totales de l'Etat</td><td align="right">
416,2</td><td align="right">
417,4</td><td align="right">
425,4</td><td align="right">
432,7</td><td align="right">
438,7</td></tr><tr><td align="left">
Evolution annuelle en volume</td><td align="left"/><td align="left"/><td align="right">
0,9 %</td><td align="right">
0,6 %</td><td align="right">
0,0 %</td></tr></table></div></div>
Sur le champ de la norme de dépenses pilotables de l'Etat (cf. définition en annexe 3), les dépenses augmenteront de 5,1 Md€ en 2018, puis de 1,6 Md€ en 2019 et de 1,0 Md€ en 2020 soit une décélération très nette du rythme d'accroissement de la dépense sous norme. Cette hausse des crédits sous norme de dépenses pilotables de 7,7 Md€ en trois ans est à comparer à la hausse de 10,4 Md€ des crédits inscrits en LFI pour 2017 sur ce périmètre par rapport à la LFI pour 2016. En outre la croissance des dépenses en 2018 s'explique en grande partie par le rebasage des impasses de budgétisation de la LFI pour 2017 mises en lumière par l'audit de la Cour des comptes de juin 2017. Les impasses documentées par la Cour des comptes au titre de la LFI 2017 s'élèvent à 4,2 Md€ (cf. tableau p.37) soit un montant proche de l'augmentation des dépenses entre les LFI 2017 et 2018.
Sur la période 2020-2022 le taux d'évolution en volume de la dépense sous norme pilotable sera de -1 % par an. Cette évolution sera rendue possible notamment par les réformes documentées dans le cadre du processus « Action Publique 2022 ».
Sur le champ de l'objectif de dépenses totales de l'Etat (cf. annexe 3), l'évolution de la dépense serait de 21 Md€ entre 2017 et 2020. Le prélèvement sur recettes à destination de l'Union européenne, compris dans le champ de cet objectif, augmenterait de 5 Md€ environ entre 2017 et 2020, avec une chronique de dépenses estimée pour les années 2018, 2019 et 2020 de 20, 23 et 24 Md€.

2. Un budget de transformation pour libérer les acteurs et les initiatives, protéger le pays et les plus vulnérables et investir dans l'avenir

a) Libérer :
Les transformations fiscales et budgétaires portées par les textes financiers sont menées en cohérence avec les réformes en matière de travail ou de logement, dans une logique de simplification et de modernisation des contraintes normatives et de libération des acteurs.
La transformation économique de la France et le soutien à l'activité et la création d'emploi passera par un soutien accru aux entreprises, notamment fiscal, avec la transformation en 2019 du CICE en allègements de charges sociales pérennes, mesure permettant à la fois une plus grande simplicité et lisibilité pour les entreprises et qui améliorera leur trésorerie dans la mesure où elles bénéficieront de l'allègement immédiatement (et non plus a posteriori comme dans le cas d'un crédit d'impôt). En parallèle la poursuite de la baisse de l'IS de 28 % à 25 % en 2022 et la suppression de la contribution de 3 % sur les dividendes viendront accroître les marges de manœuvre des entreprises et donc leur capacité à investir et à se développer.
La baisse de la pression fiscale redonnera aussi du pouvoir d'achat avec le dégrèvement de la taxe d'habitation pour 80 % des foyers, la suppression des cotisations salariales, et l'activation de dépenses sociales comme la revalorisation de la prime d'activité.
b) Protéger :
La programmation budgétaire présentée intègre le financement des priorités du Gouvernement en matière de justice, de sécurité et de défense. Ainsi la progression annuelle du budget des armées sera de + 1,7 Md€ par an tandis que 10 000 postes de policiers et de gendarmes seront créés d'ici 2022 (dont 2 000 en 2018). En outre, 6 500 postes seront créés à la justice sur la durée du quinquennat, dont 1 000 dès 2018.
La fiscalité sera aussi mise au service de la transition écologique pour protéger contre le risque climatique, avec l'alignement progressif de la fiscalité du gazole sur celle de l'essence et l'accélération de la trajectoire carbone. Des mesures budgétaires comme la généralisation du chèque-énergie et la prime à la conversion pour les véhicules anciens sont incluses pour accompagner les plus fragiles dans la transition écologique.
La trajectoire budgétaire concrétise aussi l'objectif du Gouvernement d'inventer de nouvelles protections pour mieux aider les plus fragiles. Le budget intègre ainsi les mesures de justice sociale et de soutien au pouvoir d'achat du Gouvernement que sont la revalorisation de la prime d'activité, la hausse de l'allocation aux adultes handicapés et la hausse de l'allocation de solidarité aux personnes âgées.
c) Investir :
Au-delà du soutien aux entreprises, les mesures fiscales de la trajectoire sur l'Etat visent à orienter l'épargne française vers l'investissement des entreprises qui prennent des risques, qui innovent et qui créent les emplois de demain. Ainsi un prélèvement forfaitaire unique de 30 % sur les revenus du capital sera introduit. En parallèle l'impôt sur la fortune sera supprimé et remplacé par un impôt sur la fortune immobilière. Enfin des mesures spécifiques sont prévues pour améliorer l'attractivité du territoire vis-à-vis des investissements étrangers, et notamment le développement de la place de Paris dans le contexte du Brexit.
Les crédits prévus au titre du GPI pour la période 2018-2020 sont aussi intégrés dans les plafonds de chaque mission. Ces crédits seront dotés d'une gouvernance et d'un suivi spécifique. Sera notamment créé au sein d'une nouvelle mission du budget général un fonds pour la transformation de l'action publique, doté de 700 M€ de crédits sur le quinquennat et qui financera, sur la base d'appels à projet, les coûts d'investissement nécessaires à la mise en œuvre de réformes structurelles.

3. Un budget qui opère des choix stratégiques pour financer les priorités du Gouvernement

Des économies d'ampleur seront mises en œuvre sur l'ensemble des missions du budget pour financer les priorités exposées supra. Deux secteurs pour lesquels les résultats obtenus ne sont pas à la hauteur des moyens publics engagés seront particulièrement mis à contribution :
En matière d'emploi le choix est fait, en parallèle de la réforme du code du travail, de diminuer le volume de contrats aidés, dont l'efficacité en matière de retour à l'emploi et d'insertion professionnelle n'est pas démontrée pour investir de manière prioritaire dans la formation professionnelle. Cette dépense s'inscrira dans le cadre du Grand plan d'investissement (GPI) destiné à augmenter le potentiel de croissance de l'économie française, à accélérer la transition écologique et à financer la transformation de l'action publique.
En matière de logement la baisse des aides personnelles au logement (APL) se fera dans le cadre d'une série de réformes structurelles mises en œuvre entre 2018 et 2020 qui visent à sortir de la logique inflationniste induite par le soutien à la demande de logements et à soutenir l'offre de logement.
D'autres économies seront mises en œuvre comme la rationalisation de l'intervention économique en matière de transition énergétique et le recentrage du crédit d'impôt transition énergétique sur les actions les plus efficaces.
Au-delà des économies structurelles engagées dès 2018, « Action Publique 2022 » viendra prendre le relais en initiant un processus de réforme et de modernisation des administrations,
L'évolution de la masse salariale de l'Etat et de ses opérateurs fera l'objet d'une maitrise stricte. En particulier le point d'indice est stabilisé en 2018 et un jour de carence est introduit pour réduire le micro-absentéisme. Par ailleurs, les effectifs de l'Etat et des opérateurs s'inscriront en baisse sur le quinquennat. Les créations d'emplois prévues dans les ministères stratégiques comme les armées, l'intérieur ou la justice seront ainsi compensées par des réductions d'effectifs plus importantes dans les autres domaines, selon les capacités d'optimisation et de gains de productivité identifiées.

4. Trajectoire triennale 2018-2020

La programmation d'ensemble des finances publiques couvre cinq années (2018-2022). Au cours de cette période, les moyens de l'Etat font l'objet d'une programmation plus précise qui détaille, dans le cadre du budget triennal 2018-2020, les crédits alloués à chaque mission. Les plafonds de l'année 2018 sont ceux inscrits dans le PLF. Les plafonds de l'année 2018 coïncident avec ceux qui seront présentés dans le cadre du PLF. Les plafonds de l'année 2019 sont fermes ; ceux de 2020 seront actualisés pour intégrer notamment les économies complémentaires issues du processus Action publique 2022 nécessaires au respect de la trajectoire globale.

Evolution des plafonds de crédits de paiements par mission 2017-2020

<div><div align="center"><table border="1"><tr><th>
Crédits de paiement</th><th>
LFI 2017</th><th>
LFI 2017 Format 2018</th><th>
2018</th><th>
2019</th><th>
2020</th></tr><tr><td align="left" valign="middle">
Action et transformation publiques</td><td align="center" valign="middle">
0,00</td><td align="center" valign="middle">
0,00</td><td align="center" valign="middle">
0,02</td><td align="center" valign="middle">
0,28</td><td align="center" valign="middle">
0,55</td></tr><tr><td align="left" valign="middle">
Action extérieure de l'Etat</td><td align="center" valign="middle">
2,86</td><td align="center" valign="middle">
2,86</td><td align="center" valign="middle">
2,86</td><td align="center" valign="middle">
2,75</td><td align="center" valign="middle">
2,69</td></tr><tr><td align="left" valign="middle">
Administration générale et territoriale de l'Etat</td><td align="center" valign="middle">
2,49</td><td align="center" valign="middle">
2,50</td><td align="center" valign="middle">
2,15</td><td align="center" valign="middle">
2,14</td><td align="center" valign="middle">
2,30</td></tr><tr><td align="left" valign="middle">
Agriculture, alimentation, forêt et affaires rurales</td><td align="center" valign="middle">
3,15</td><td align="center" valign="middle">
2,79</td><td align="center" valign="middle">
3,18</td><td align="center" valign="middle">
2,88</td><td align="center" valign="middle">
2,84</td></tr><tr><td align="left" valign="middle">
Aide publique au développement</td><td align="center" valign="middle">
2,58</td><td align="center" valign="middle">
2,59</td><td align="center" valign="middle">
2,68</td><td align="center" valign="middle">
2,81</td><td align="center" valign="middle">
3,10</td></tr><tr><td align="left" valign="middle">
Anciens combattants, mémoire et liens avec la nation</td><td align="center" valign="middle">
2,54</td><td align="center" valign="middle">
2,54</td><td align="center" valign="middle">
2,46</td><td align="center" valign="middle">
2,34</td><td align="center" valign="middle">
2,25</td></tr><tr><td align="left" valign="middle">
Cohésion des territoires</td><td align="center" valign="middle">
18,26</td><td align="center" valign="middle">
18,26</td><td align="center" valign="middle">
17,22</td><td align="center" valign="middle">
15,65</td><td align="center" valign="middle">
15,14</td></tr><tr><td align="left" valign="middle">
Conseil et contrôle de l'Etat</td><td align="center" valign="middle">
0,51</td><td align="center" valign="middle">
0,51</td><td align="center" valign="middle">
0,52</td><td align="center" valign="middle">
0,53</td><td align="center" valign="middle">
0,53</td></tr><tr><td align="left" valign="middle">
Crédits non répartis</td><td align="center" valign="middle">
0,02</td><td align="center" valign="middle">
0,02</td><td align="center" valign="middle">
0,12</td><td align="center" valign="middle">
0,12</td><td align="center" valign="middle">
0,85</td></tr><tr><td align="left" valign="middle">
Culture</td><td align="center" valign="middle">
2,70</td><td align="center" valign="middle">
2,70</td><td align="center" valign="middle">
2,72</td><td align="center" valign="middle">
2,74</td><td align="center" valign="middle">
2,78</td></tr><tr><td align="left" valign="middle">
Défense</td><td align="center" valign="middle">
32,44</td><td align="center" valign="middle">
32,44</td><td align="center" valign="middle">
34,20</td><td align="center" valign="middle">
35,90</td><td align="center" valign="middle">
37,60</td></tr><tr><td align="left" valign="middle">
Direction de l'action du Gouvernement</td><td align="center" valign="middle">
1,37</td><td align="center" valign="middle">
1,38</td><td align="center" valign="middle">
1,38</td><td align="center" valign="middle">
1,39</td><td align="center" valign="middle">
1,40</td></tr><tr><td align="left" valign="middle">
Ecologie, développement et mobilité durables</td><td align="center" valign="middle">
9,44</td><td align="center" valign="middle">
9,91</td><td align="center" valign="middle">
10,39</td><td align="center" valign="middle">
10,55</td><td align="center" valign="middle">
10,57</td></tr><tr><td align="left" valign="middle">
Economie</td><td align="center" valign="middle">
1,64</td><td align="center" valign="middle">
1,65</td><td align="center" valign="middle">
1,62</td><td align="center" valign="middle">
1,79</td><td align="center" valign="middle">
2,15</td></tr><tr><td align="left" valign="middle">
Engagements financiers de l'Etat (hors dette)</td><td align="center" valign="middle">
0,55</td><td align="center" valign="middle">
0,55</td><td align="center" valign="middle">
0,58</td><td align="center" valign="middle">
0,43</td><td align="center" valign="middle">
0,43</td></tr><tr><td align="left" valign="middle">
Enseignement scolaire</td><td align="center" valign="middle">
50,01</td><td align="center" valign="middle">
50,01</td><td align="center" valign="middle">
51,49</td><td align="center" valign="middle">
52,09</td><td align="center" valign="middle">
52,95</td></tr><tr><td align="left" valign="middle">
Gestion des finances publiques et des ressources humaines</td><td align="center" valign="middle">
8,12</td><td align="center" valign="middle">
8,11</td><td align="center" valign="middle">
8,15</td><td align="center" valign="middle">
8,10</td><td align="center" valign="middle">
8,04</td></tr><tr><td align="left" valign="middle">
Immigration, asile et intégration</td><td align="center" valign="middle">
1,10</td><td align="center" valign="middle">
1,10</td><td align="center" valign="middle">
1,38</td><td align="center" valign="middle">
1,36</td><td align="center" valign="middle">
1,36</td></tr><tr><td align="left" valign="middle">
Investissements d'avenir</td><td align="center" valign="middle">
0,00</td><td align="center" valign="middle">
0,00</td><td align="center" valign="middle">
1,08</td><td align="center" valign="middle">
1,05</td><td align="center" valign="middle">
1,88</td></tr><tr><td align="left" valign="middle">
Justice</td><td align="center" valign="middle">
6,85</td><td align="center" valign="middle">
6,72</td><td align="center" valign="middle">
6,98</td><td align="center" valign="middle">
7,29</td><td align="center" valign="middle">
7,65</td></tr><tr><td align="left" valign="middle">
Médias, livre et industries culturelles</td><td align="center" valign="middle">
0,57</td><td align="center" valign="middle">
0,57</td><td align="center" valign="middle">
0,55</td><td align="center" valign="middle">
0,54</td><td align="center" valign="middle">
0,54</td></tr><tr><td align="left" valign="middle">
Outre-mer</td><td align="center" valign="middle">
2,02</td><td align="center" valign="middle">
2,02</td><td align="center" valign="middle">
2,02</td><td align="center" valign="middle">
2,02</td><td align="center" valign="middle">
2,03</td></tr><tr><td align="left" valign="middle">
Pouvoirs publics</td><td align="center" valign="middle">
0,99</td><td align="center" valign="middle">
0,99</td><td align="center" valign="middle">
0,99</td><td align="center" valign="middle">
0,99</td><td align="center" valign="middle">
0,99</td></tr><tr><td align="left" valign="middle">
Recherche et enseignement supérieur</td><td align="center" valign="middle">
26,69</td><td align="center" valign="middle">
26,69</td><td align="center" valign="middle">
27,40</td><td align="center" valign="middle">
27,87</td><td align="center" valign="middle">
28,02</td></tr><tr><td align="left" valign="middle">
Régimes sociaux et de retraite</td><td align="center" valign="middle">
6,31</td><td align="center" valign="middle">
6,31</td><td align="center" valign="middle">
6,33</td><td align="center" valign="middle">
6,27</td><td align="center" valign="middle">
6,30</td></tr><tr><td align="left" valign="middle">
Relations avec les collectivités territoriales</td><td align="center" valign="middle">
3,44</td><td align="center" valign="middle">
3,35</td><td align="center" valign="middle">
3,66</td><td align="center" valign="middle">
3,51</td><td align="center" valign="middle">
3,54</td></tr><tr><td align="left" valign="middle">
Santé</td><td align="center" valign="middle">
1,27</td><td align="center" valign="middle">
1,24</td><td align="center" valign="middle">
1,38</td><td align="center" valign="middle">
1,48</td><td align="center" valign="middle">
1,54</td></tr><tr><td align="left" valign="middle">
Sécurités</td><td align="center" valign="middle">
13,10</td><td align="center" valign="middle">
13,09</td><td align="center" valign="middle">
13,32</td><td align="center" valign="middle">
13,48</td><td align="center" valign="middle">
13,66</td></tr><tr><td align="left" valign="middle">
Solidarité, insertion et égalité des chances</td><td align="center" valign="middle">
17,64</td><td align="center" valign="middle">
17,67</td><td align="center" valign="middle">
19,44</td><td align="center" valign="middle">
21,31</td><td align="center" valign="middle">
21,94</td></tr><tr><td align="left" valign="middle">
Sport, jeunesse et vie associative</td><td align="center" valign="middle">
0,73</td><td align="center" valign="middle">
0,80</td><td align="center" valign="middle">
0,95</td><td align="center" valign="middle">
1,05</td><td align="center" valign="middle">
1,07</td></tr><tr><td align="left" valign="middle">
Travail et emploi</td><td align="center" valign="middle">
15,27</td><td align="center" valign="middle">
16,68</td><td align="center" valign="middle">
15,17</td><td align="center" valign="middle">
12,96</td><td align="center" valign="middle">
12,68</td></tr></table></div></div>
Le respect de ce budget triennal est garanti par deux mécanismes fondamentaux : un principe d'auto-assurance et la constitution d'une réserve de précaution.

B. - La trajectoire des organismes divers d'administration centrale

Eu égard à la règle posée à l'article 12 de la loi de programmation des finances publiques 2011-2014 interdisant aux ODAC de s'endetter auprès d'un établissement de crédit pour une période allant au-delà d'une année les ODAC sont globalement à l'équilibre. La poursuite des décaissements au titre des PIA 1 et 2 explique le solde déficitaire des ODAC sur la période.

Tableau : trajectoire des organismes divers d'administration centrale

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Dépense</td><td align="center" valign="middle">
3,5</td><td align="center" valign="middle">
3,3</td><td align="center" valign="middle">
3,2</td><td align="center" valign="middle">
3,1</td><td align="center" valign="middle">
3,0</td><td align="center" valign="middle">
3,0</td></tr><tr><td align="left" valign="middle">
Recette</td><td align="center" valign="middle">
3,5</td><td align="center" valign="middle">
3,2</td><td align="center" valign="middle">
3,1</td><td align="center" valign="middle">
3,0</td><td align="center" valign="middle">
3,0</td><td align="center" valign="middle">
2,9</td></tr><tr><td align="left" valign="middle">
Solde</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,0</td></tr></table></div></div>
C. - La trajectoire des administrations de sécurité sociale

Le tableau ci-dessous présente la trajectoire des administrations de sécurité sociale pour les années 2016-2022.

<div><div align="center"><table border="1"><tr><th>
(En points de PIB sauf indication contraire)</th><th>
2016</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Dépenses</td><td align="center" valign="middle">
26,3</td><td align="center" valign="middle">
26,0</td><td align="center" valign="middle">
25,9</td><td align="center" valign="middle">
25,6</td><td align="center" valign="middle">
25,1</td><td align="center" valign="middle">
24,9</td><td align="center" valign="middle">
24,5</td></tr><tr><td align="left" valign="middle">
Recettes</td><td align="center" valign="middle">
26,1</td><td align="center" valign="middle">
26,2</td><td align="center" valign="middle">
26,5</td><td align="center" valign="middle">
26,4</td><td align="center" valign="middle">
25,9</td><td align="center" valign="middle">
25,7</td><td align="center" valign="middle">
25,3</td></tr><tr><td align="left" valign="middle">
Solde</td><td align="center" valign="middle">
- 0,1</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,8</td><td align="center" valign="middle">
0,8</td><td align="center" valign="middle">
0,8</td><td align="center" valign="middle">
0,8</td></tr><tr><td align="left" valign="middle">
Solde (Md€)</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
4,0</td><td align="center" valign="middle">
12,5</td><td align="center" valign="middle">
19,9</td><td align="center" valign="middle">
20,3</td><td align="center" valign="middle">
20,9</td><td align="center" valign="middle">
21,7</td></tr></table></div></div>
Le solde des administrations de sécurité sociale (ASSO) se redresserait sur la période 2019-2022, du fait de la dynamique modérée des dépenses des branches vieillesse et famille et de la maîtrise des dépenses d'assurance maladie. Proche de l'équilibre dans le compte provisoire de 2016, le solde des ASSO deviendrait positif dès 2017. Au-delà, sous le double impact de dépenses maîtrisées et de recettes favorables dans un contexte de croissance solide et de nette amélioration en termes d'emploi, le solde des ASSO deviendrait très excédentaire.
Aussi, dans cette trajectoire, il est fait l'hypothèse conventionnelle d'une contribution du secteur ASSO à la réduction du déficit de l'Etat, sous forme de transfert, dès 2019. Ce transfert stabilise l'excédent ASSO hors Cades et FRR sur toute la période à un niveau légèrement supérieur à l'équilibre. Ce choix, neutre sur l'ensemble des APU, reflète aussi le fait qu'une contribution des sous-secteurs revenus à l'équilibre reste nécessaire.
Les comptes présentés dans la loi de financement de la sécurité sociale et dans la loi de programmation des finances publiques relèvent de périmètres et de conventions comptables distinctes à double titre. D'une part, le champ des ASSO couvert par la LPFP est plus étendu que celui couvert par la LFSS car il inclut notamment le régime d'indemnisation du chômage, les régimes complémentaires de retraite des salariés et les organismes dépendant des assurances sociales (principalement les hôpitaux et Pôle Emploi). D'autre part, les conventions comptables utilisées par la comptabilité nationale diffèrent du plan comptable unique des organismes de sécurité sociale. Elles excluent en particulier les dotations nettes des reprises sur provisions, les dotations aux amortissements et les plus-values sur cessions d'immobilisations financières ou opérations de change.

1. Maîtriser dans un cadre pluriannuel les dépenses d'assurance-maladie tout en garantissant la qualité des soins et l'accès à l'innovation

La gouvernance de l'ONDAM mise en place depuis plusieurs années (abaissement du seuil d'alerte, mise en place du comité de pilotage, augmentation du nombre des interventions du comité d'alerte) ainsi que la mise en œuvre du plan d'économies 2015-2017 ont contribué au respect de l'objectif en 2016 pour la septième année consécutive. S'agissant de 2017, les données d'exécution à fin mai sur les soins de ville ne laissent pas, à ce stade, craindre un dépassement de l'objectif pour 2017, constat qui avait déjà été celui du comité d'alerte de l'ONDAM en juin. Ce dernier se réunira au plus tard le 15 octobre pour rendre un avis qui portera à la fois sur la construction de l'objectif pour 2018 et sur les risques éventuels quant à la réalisation de l'objectif de l'année en cours.
Dans le cadre de la stratégie globale des finances publiques, l'ONDAM connaîtra un taux d'évolution en moyenne annuelle de + 2,3 % sur la période 2018-2020, soit un effort d'économies de 4,2 Md€ dès 2018 afin de compenser une évolution tendancielle des dépenses de + 4,5 %.
Pour assurer le respect de cette trajectoire, assurer une amélioration continue de la qualité de soins et garantir l'accès de tous aux soins innovants, le système de soins poursuivra sa transformation dans le cadre de la stratégie nationale de santé (SNS). La prévention, la lutte contre les inégalités d'accès sociales et territoriales aux soins, la pertinence et la qualité des soins et le soutien à l'innovation en constitueront les axes prioritaires d'action.
L'ensemble des acteurs en ville et à l'hôpital seront mobilisés durant la période 2018-2022 pour renforcer l'efficience de l'offre de soins avec comme leviers la structuration de parcours de soins efficients, l'amélioration de la performance des établissements de santé et médico-sociaux, la pertinence des actes, des prestations et des produits de santé.
Les efforts sur les prix des médicaments seront poursuivis pour rémunérer l'innovation à son juste prix et développer l'usage des génériques et des biosimilaires avec pour objectif de maintenir un haut niveau d'accès aux soins innovants tout en assurant la soutenabilité de la dépense.

2. La modération des dépenses des branches vieillesse et famille contribuera à l'objectif global de redressement des finances publiques

Le tableau ci-dessous présente les dépenses prévisionnelles d'assurance vieillesse et d'allocations familiales des régimes obligatoires de base de sécurité sociale pour les années 2018-2020 :

Charges nettes des régimes obligatoires de base de sécurité sociale

<div><div align="center"><table border="1"><tr><th>
En évolution</th><th>
2018</th><th>
2019</th><th>
2020</th></tr><tr><td align="left" valign="middle">
Branche vieillesse</td><td align="left" valign="middle">
2,3 %</td><td align="left" valign="middle">
2,5 %</td><td align="left" valign="middle">
2,6 %</td></tr><tr><td align="left" valign="middle">
Branche famille</td><td align="left" valign="middle">
0,1 %</td><td align="left" valign="middle">
0,8 %</td><td align="left" valign="middle">
0,9 %</td></tr><tr><td align="left" valign="middle">
ONDAM</td><td align="left" valign="middle">
2,3 %</td><td align="left" valign="middle">
2,3 %</td><td align="left" valign="middle">
2,3 %</td></tr></table></div></div>
L'évolution des dépenses d'assurance vieillesse des régimes de base serait de + 2,7 % en moyenne sur la période 2018-2022. La reprise progressive de l'inflation entraîne une accélération très progressive des pensions. Toutefois celle-ci resterait assez modérée, du fait de la montée en charge jusqu'en 2022 de la réforme des retraites de 2010.
Comme pour les retraites, les dépenses indexées de la branche famille devraient subir l'effet de la reprise de l'inflation. Toutefois, la trajectoire des dépenses comporte un volet d'économies conformément aux dispositions qui sont proposées au vote du Parlement dans le projet de loi de financement de la sécurité sociale pour 2018. Ces dispositions permettront d'infléchir les dépenses de la branche via, notamment, une meilleure priorisation au sein des dépenses du Fonds national d'action sociale (FNAS) de la CNAF. Ainsi, les dépenses connaîtraient une évolution moyenne sur la trajectoire de 0,8 %.

3. Le dynamisme de la masse salariale et les accords conclus par les partenaires sociaux permettront de garantir la pérennité des régimes complémentaires de retraite

Le solde des régimes complémentaires se redresserait de 7,1 Md€ entre 2016 et 2022. Cette amélioration s'explique notamment par les efforts entrepris par les régimes Agirc et Arrco dans le cadre de l'accord signé en 2015. Sa mise en œuvre soutiendrait les recettes (augmentation du taux d'appel à 127 %, fusion de la tranche T2 et de la tranche TB, unification des deux régimes) et permettrait aux dépenses de ralentir (via les coefficients de solidarité et les coefficients majorants notamment).
Dépenses, recettes et soldes des régimes complémentaires de retraite :

<div><div align="center"><table border="1"><tr><th>
En Md€ en comptabilité nationale</th><th>
2018</th><th>
2019</th><th>
2020</th></tr><tr><td align="left" valign="middle">
Solde</td><td align="center" valign="middle">
1,6</td><td align="center" valign="middle">
4,5</td><td align="center" valign="middle">
5,5</td></tr><tr><td align="left" valign="middle">
Recettes</td><td align="center" valign="middle">
102,9</td><td align="center" valign="middle">
107,8</td><td align="center" valign="middle">
111,4</td></tr><tr><td align="left" valign="middle">
Dépenses</td><td align="center" valign="middle">
101,2</td><td align="center" valign="middle">
103,4</td><td align="center" valign="middle">
106,0</td></tr></table></div></div>
4. La baisse du chômage, associée aux effets de la convention de mars 2017, permettra le retour à l'équilibre de l'UNEDIC

Le solde du régime d'indemnisation du chômage se redresserait significativement entre 2016 et 2022 sous l'effet de l'amélioration progressive de l'emploi et des mesures prises par les partenaires sociaux lors des accords professionnels du 28 mars 2017. Ceux-ci permettraient des économies progressives en dépenses sur la période, allant de 260 M€ en 2018 jusqu'à un peu plus de 900 M€ en 2022. Par ailleurs, des mesures temporaires en recettes soutiendraient l'amélioration du solde en début de période (pour environ 300 M€ en 2018 et 2019) avant de s'éteindre progressivement.
Dans le cadre de la mesure de baisse des cotisations pour les actifs en 2018, les cotisations salariales à l'assurance chômage seront supprimées (baisse de 2,4 points). Cette mesure sera intégralement compensée pour l'UNEDIC.
Afin de rendre le marché du travail plus fluide, l'assurance chômage deviendra universelle progressivement à partir de 2018 : elle couvrira tous les actifs (salariés, travailleurs indépendants) et facilitera les transitions d'un statut à un autre. En contrepartie de ces droits nouveaux, le contrôle de la recherche d'emploi sera accru avec le recrutement de mille conseillers de contrôle. Le Gouvernement engagera dès l'automne 2017 une concertation avec les partenaires sociaux gestionnaires de l'assurance chômage sur l'ensemble de ces mesures qui devront également permettre de garantir la soutenabilité financière du régime.

Dépenses, recettes et soldes de l'assurance chômage

<div><div align="center"><table border="1"><tr><th>
Md€ en comptabilité nationale</th><th>
2018</th><th>
2019</th><th>
2020</th></tr><tr><td align="left" valign="middle">
Solde</td><td align="center" valign="middle">
- 2,1</td><td align="center" valign="middle">
- 0,8</td><td align="center" valign="middle">
0,6</td></tr><tr><td align="left" valign="middle">
Recettes</td><td align="center" valign="middle">
37,8</td><td align="center" valign="middle">
38,9</td><td align="center" valign="middle">
40,1</td></tr><tr><td align="left" valign="middle">
Dépenses</td><td align="center" valign="middle">
39,9</td><td align="center" valign="middle">
39,7</td><td align="center" valign="middle">
39,4</td></tr></table></div></div>
D. - La trajectoire des administrations publiques locales

Comme les autres secteurs publics, les administrations publiques locales contribueront à l'effort de maîtrise des dépenses publiques : leurs dépenses diminueront d'environ 1 point de PIB sur l'ensemble du quinquennat, passant de 11,2 % en 2017 à 10,1 % en 2022, tandis que leurs recettes diminueront dans des proportions moindres (de 11,2 % de PIB en 2017 à 10,8 % en 2022). Ainsi, leur solde s'améliorera fortement au cours des prochaines années, passant de 0,1 % de PIB en 2017 (soit 1,4 Md€) à 0,7 % de PIB en 2022 (soit 19,5 Md€).
Cette trajectoire est une conséquence directe du nouveau pacte financier entre l'Etat et les collectivités locales : celles-ci s'engageront à baisser leurs dépenses de 13 Md€ sur la durée du quinquennat par rapport à leur évolution tendancielle mais ne verront pas leurs dotations réduites. Ce contrat de mandature traduit la volonté du Président de la République de modifier en profondeur le rapport de l'Etat et des collectivités territoriales : ce dernier sera fondé sur la confiance et la responsabilité et non plus sur la contrainte. Au-delà de cette dynamique d'ensemble, le solde des collectivités locales sera marqué de manière usuelle par le cycle électoral communal qui affecte habituellement leurs dépenses d'investissement. L'investissement local devrait ainsi connaitre une hausse marquée jusqu'en 2019 puis une baisse à partir de 2020, année d'élection municipale.

Tableau : trajectoire des administrations publiques locales

<div><div align="center"><table border="1"><tr><th>
(% PIB)</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Dépenses</td><td align="center" valign="middle">
11,2</td><td align="center" valign="middle">
11,0</td><td align="center" valign="middle">
10,9</td><td align="center" valign="middle">
10,7</td><td align="center" valign="middle">
10,3</td><td align="center" valign="middle">
10,1</td></tr><tr><td align="left" valign="middle">
Recettes</td><td align="center" valign="middle">
11,2</td><td align="center" valign="middle">
11,1</td><td align="center" valign="middle">
11,0</td><td align="center" valign="middle">
10,9</td><td align="center" valign="middle">
10,9</td><td align="center" valign="middle">
10,8</td></tr><tr><td align="left" valign="middle">
Solde</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,1</td><td align="center" valign="middle">
0,3</td><td align="center" valign="middle">
0,5</td><td align="center" valign="middle">
0,7</td></tr><tr><td align="left" valign="middle">
Solde (Md€)</td><td align="center" valign="middle">
1,4</td><td align="center" valign="middle">
1,7</td><td align="center" valign="middle">
2,7</td><td align="center" valign="middle">
6,8</td><td align="center" valign="middle">
14,2</td><td align="center" valign="middle">
19,5</td></tr></table></div></div>
E. - Si la législation et les politiques de finances publiques n'étaient pas réformées, le déficit public se résorberait plus lentement et la dette ne décroîtrait pas sur le quinquennat

La trajectoire de solde public à législation et pratique inchangées, présentée ici conformément à la loi organique de programmation et de gouvernance des finances publiques de 2012 et à la directive de 2011 sur les cadres budgétaires, s'appuie sur les résultats obtenus ces dernières années en termes de maîtrise de la dépense publique, et prend pour hypothèse une croissance des dépenses égale à la moyenne sur les 10 dernières années de la croissance publique en volume hors crédits d'impôt (soit + 1,3 % par an).
En recettes, elle se place dans un cadre dans lequel aucune mesure nouvelle n'aurait été prise depuis le début de la nouvelle législature (été 2017). Dans un tel scénario, le déficit se résorberait lentement : en 2022, il serait toujours de 2,2 % de PIB contre 0,3 % dans la trajectoire de la LPFP. De ce fait, le ratio de dette publique sur PIB serait beaucoup plus dynamique : la dette culminerait à 97,9 % point de PIB en 2019, puis commencerait à refluer lentement pour atteindre 96,6 % de PIB en 2022. Sur le quinquennat, la dette serait quasi-inchangée. A l'inverse, dans la trajectoire de la LPFP, la dette atteint 97,1 % de PIB en 2019 et décroît ensuite, pour s'établir à 91,4 % en 2022.

<div><div align="center"><table border="1"><tr><th>
En % PIB</th><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Solde public et législation et pratique inchangées</td><td align="center" valign="middle">
- 3,1</td><td align="center" valign="middle">
- 3,4</td><td align="center" valign="middle">
- 2,8</td><td align="center" valign="middle">
- 2,5</td><td align="center" valign="middle">
- 2,4</td><td align="center" valign="middle">
- 2,2</td></tr><tr><td align="left" valign="middle">
Dette publique à législation et pratique inchangées</td><td align="center" valign="middle">
96,9</td><td align="center" valign="middle">
97,7</td><td align="center" valign="middle">
97,9</td><td align="center" valign="middle">
97,8</td><td align="center" valign="middle">
97,4</td><td align="center" valign="middle">
96,6</td></tr><tr><td align="left" valign="middle">
Dépenses hors CI</td><td align="center" valign="middle">
0,2</td><td align="center" valign="middle">
0,6</td><td align="center" valign="middle">
0,9</td><td align="center" valign="middle">
1,4</td><td align="center" valign="middle">
2,0</td><td align="center" valign="middle">
2,6</td></tr><tr><td align="left" valign="middle">
Mesures annoncées en recettes (*)</td><td align="left" valign="middle"/><td align="center" valign="middle">
0,0</td><td align="center" valign="middle">
- 1,0</td><td align="center" valign="middle">
- 0,4</td><td align="center" valign="middle">
- 0,5</td><td align="center" valign="middle">
- 0,6</td></tr><tr><td align="left" valign="middle">
Solde public de la LPFP</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 2,8</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 1,5</td><td align="center" valign="middle">
- 0,9</td><td align="center" valign="middle">
- 0,3</td></tr><tr><td align="left" valign="middle">
Dette publique de la LPFP</td><td align="center" valign="middle">
96,7</td><td align="center" valign="middle">
96,9</td><td align="center" valign="middle">
97,1</td><td align="center" valign="middle">
96,1</td><td align="center" valign="middle">
94,2</td><td align="center" valign="middle">
91,4</td></tr><tr><td align="left" colspan="7" valign="middle">
(*) yc contribution nette des crédits d'impôts en recettes et en dépenses</td></tr></table></div></div>
