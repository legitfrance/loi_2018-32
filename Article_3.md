Dans le contexte macroéconomique et selon les hypothèses et les méthodes retenues pour établir la programmation mentionnée à l'article 2 :
1° L'évolution du solde public effectif, du solde conjoncturel, des mesures ponctuelles et temporaires, du solde structurel et de la dette publique s'établit comme suit :

(En points de produit intérieur brut)

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left">
Solde public effectif (1 + 2 + 3)</td><td align="right">
- 2,9</td><td align="right">
- 2,8</td><td align="right">
- 2,9</td><td align="right">
- 1,5</td><td align="right">
- 0,9</td><td align="right">
- 0,3</td></tr><tr><td align="left">
Solde conjoncturel (1)</td><td align="right">
- 0,6</td><td align="right">
- 0,4</td><td align="right">
- 0,1</td><td align="right">
0,1</td><td align="right">
0,3</td><td align="right">
0,6</td></tr><tr><td align="left">
Mesures ponctuelles et temporaires (2)</td><td align="right">
- 0,1</td><td align="right">
- 0,2</td><td align="right">
- 0,9</td><td align="right">
0,0</td><td align="right">
0,0</td><td align="right">
0,0</td></tr><tr><td align="left">
Solde structurel (en points de PIB potentiel) (3)</td><td align="right">
- 2,2</td><td align="right">
- 2,1</td><td align="right">
- 1,9</td><td align="right">
- 1,6</td><td align="right">
- 1,2</td><td align="right">
- 0,8</td></tr><tr><td align="left">
Dette des administrations publiques</td><td align="right">
96,7</td><td align="right">
96,9</td><td align="right">
97,1</td><td align="right">
96,1</td><td align="right">
94,2</td><td align="right">
91,4</td></tr></table></div></div>
2° L'évolution du solde public effectif, décliné par sous-secteur des administrations publiques, s'établit comme suit :

(En points de produit intérieur brut)

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left">
Solde public effectif</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 2,8</td><td align="center" valign="middle">
- 2,9</td><td align="center" valign="middle">
- 1,5</td><td align="center" valign="middle">
- 0,9</td><td align="center" valign="middle">
- 0,3</td></tr><tr><td align="left">
Dont :</td><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/><td align="left" valign="middle"/></tr><tr><td align="left">
- administrations publiques centrales</td><td align="center">
- 3,2</td><td align="center">
- 3,4</td><td align="center">
- 3,9</td><td align="center">
- 2,6</td><td align="center">
- 2,3</td><td align="center">
- 1,8</td></tr><tr><td align="left">
- administrations publiques locales</td><td align="center">
0,1</td><td align="center">
0,1</td><td align="center">
0,1</td><td align="center">
0,3</td><td align="center">
0,5</td><td align="center">
0,7</td></tr><tr><td align="left">
- administrations de sécurité sociale</td><td align="center">
0,2</td><td align="center">
0,5</td><td align="center">
0,8</td><td align="center">
0,8</td><td align="center">
0,8</td><td align="center">
0,8</td></tr></table></div></div>
