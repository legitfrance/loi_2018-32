L'incidence des mesures afférentes aux prélèvements obligatoires adoptées par le Parlement ou prises par le Gouvernement par voie réglementaire à compter du 1er juillet 2017 ne peut être inférieure aux montants suivants, exprimés en milliards d'euros courants :

<div><div align="center"><table border="1"><tr><th>
2018</th><th>
2019</th><th>
2020</th></tr><tr><td align="center" valign="middle">
- 5</td><td align="center" valign="middle">
- 9</td><td align="center" valign="middle">
- 7</td></tr></table></div></div>
L'incidence mentionnée au premier alinéa est appréciée, une année, donnée, au regard de la situation de l'année précédente.
