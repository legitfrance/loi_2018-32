Dans le contexte macroéconomique mentionné à l'article 2, les objectifs d'évolution de la dépense publique des sous-secteurs des administrations publiques s'établissent comme suit :

Taux de croissance des dépenses publiques en volume, hors crédits d'impôt et transferts, corrigées des changements de périmètre

(En %)

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Administrations publiques, hors crédits d'impôt </td><td align="center">
0,9</td><td align="center">
0,6</td><td align="center">
0,7</td><td align="center">
0,3</td><td align="center">
0,2</td><td align="center">
0,1</td></tr><tr><td align="left" valign="middle">
Dont :</td><td align="left"/><td align="left"/><td align="left"/><td align="left"/><td align="left"/><td align="left"/></tr><tr><td align="left" valign="middle">
- administrations publiques centrales</td><td align="center">
1,0</td><td align="center">
0,3</td><td align="center">
0,8</td><td align="center">
1,2</td><td align="center">
0,7</td><td align="center">
0,2</td></tr><tr><td align="left" valign="middle">
- administrations publiques locales</td><td align="center">
1,7</td><td align="center">
0,2</td><td align="center">
0,9</td><td align="center">
- 0,4</td><td align="center">
- 1,6</td><td align="center">
- 0,6</td></tr><tr><td align="left" valign="middle">
- administrations de sécurité sociale</td><td align="center">
0,6</td><td align="center">
0,9</td><td align="center">
0,4</td><td align="center">
0,1</td><td align="center">
0,6</td><td align="center">
0,4</td></tr><tr><td align="left" valign="middle">
Administrations publiques, y compris crédits d'impôt</td><td align="center">
1,0</td><td align="center">
1,0</td><td align="center">
0,5</td><td align="center">
- 1,2</td><td align="center">
0,1</td><td align="center">
0,1</td></tr><tr><td align="left" valign="middle">
Dont administrations publiques centrales</td><td align="center">
1,0</td><td align="center">
1,4</td><td align="center">
0,3</td><td align="center">
- 3,2</td><td align="center">
0,3</td><td align="center">
0,2</td></tr></table></div></div>
