Le Gouvernement présente au Parlement, en annexe au projet de loi de finances de l'année, un rapport relatif au « Grand plan d'investissement », jusqu'à la consommation de l'ensemble des crédits inscrits pour ces investissements, et qui comprend :

- la récapitulation des crédits consacrés au plan, par mission, programme et action, au cours des trois précédents exercices, de la prévision d'exécution pour l'exercice en cours et de la prévision pour les trois années à venir, en distinguant les crédits redéployés des crédits nouveaux ainsi que les moyens de financement, suivant qu'il s'agit de crédits budgétaires ou d'instruments financiers ;
- un bilan détaillé des mesures financées au titre de ce plan pour l'ensemble des administrations publiques ;
- une présentation exhaustive et par année des modifications apportées à la répartition initiale des crédits ;
- une présentation, pour les trois exercices précédents, en cours et à venir, des conséquences sur les finances publiques des investissements financés par les crédits relevant du plan, en particulier leurs conséquences sur le montant des dépenses publiques, des recettes publiques, du déficit public et de la dette publique, en précisant les administrations publiques concernées ;
- les résultats attendus et obtenus, mesurés au moyen d'indicateurs précis dont le choix est justifié ;
- une présentation des dispositifs de sélection des projets et programmes financés dans le cadre de ce plan ainsi que des méthodes d'évaluation retenues pour mesurer les résultats obtenus.

Ce rapport est déposé sur le bureau des assemblées parlementaires et distribué au moins cinq jours francs avant l'examen par l'Assemblée nationale, en première lecture, des crédits de la première des missions concernées.
