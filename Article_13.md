I. - Les collectivités territoriales contribuent à l'effort de réduction du déficit public et de maîtrise de la dépense publique, selon des modalités à l'élaboration desquelles elles sont associées.
II. - A l'occasion du débat sur les orientations budgétaires, chaque collectivité territoriale ou groupement de collectivités territoriales présente ses objectifs concernant :
1° L'évolution des dépenses réelles de fonctionnement, exprimées en valeur, en comptabilité générale de la section de fonctionnement ;
2° L'évolution du besoin de financement annuel calculé comme les emprunts minorés des remboursements de dette.
Ces éléments prennent en compte les budgets principaux et l'ensemble des budgets annexes.
III. - L'objectif national d'évolution des dépenses réelles de fonctionnement des collectivités territoriales et de leurs groupements à fiscalité propre correspond à un taux de croissance annuel de 1,2 % appliqué à une base de dépenses réelles de fonctionnement en 2017, en valeur et à périmètre constant. Pour une base 100 en 2017, cette évolution s'établit selon l'indice suivant :

<div><div align="center"><table border="1"><tr><th>
Collectivités territoriales et groupements à fiscalité propre</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left">
Dépenses de fonctionnement</td><td align="center" valign="middle">
101,2</td><td align="center" valign="middle">
102,4</td><td align="center" valign="middle">
103,6</td><td align="center" valign="middle">
104,9</td><td align="center" valign="middle">
106,2</td></tr></table></div></div>
IV. - L'objectif national d'évolution du besoin annuel de financement des collectivités territoriales et de leurs groupements à fiscalité propre, s'établit comme suit, en milliards d'euros courants :

(En milliards d'euros)

<div><div align="center"><table border="1"><tr><th>
Collectivités territoriales et groupements à fiscalité propre</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left">
Réduction annuelle du besoin de financement</td><td align="right">
- 2,6</td><td align="right">
- 2,6</td><td align="right">
- 2,6</td><td align="right">
- 2,6</td><td align="right">
- 2,6</td></tr><tr><td align="left">
Réduction cumulée du besoin de financement</td><td align="right">
- 2,6</td><td align="right">
- 5,2</td><td align="right">
- 7,8</td><td align="right">
- 10,4</td><td align="right">
- 13</td></tr></table></div></div>
