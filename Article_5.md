Dans le contexte macroéconomique et selon les hypothèses et les méthodes retenues pour établir la programmation mentionnée à l'article 2, les objectifs d'évolution de la dépense publique et du taux de prélèvements obligatoires s'établissent comme suit :

(En points de produit intérieur brut)

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left">
Dépense publique, hors crédits d'impôts</td><td align="center">
54,7</td><td align="center">
54,0</td><td align="center">
53,4</td><td align="center">
52,6</td><td align="center">
51,9</td><td align="center">
51,1</td></tr><tr><td align="left">
Taux de prélèvements obligatoires</td><td align="center">
44,7</td><td align="center">
44,3</td><td align="center">
43,4</td><td align="center">
43,7</td><td align="center">
43,7</td><td align="center">
43,7</td></tr><tr><td align="left">
Dépenses publiques, y compris crédits d'impôts</td><td align="center" valign="middle">
56,1</td><td align="center">
55,7</td><td align="center">
54,9</td><td align="center">
53,3</td><td align="center">
52,5</td><td align="center">
51,6</td></tr></table></div></div>
