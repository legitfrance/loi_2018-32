L'objectif d'effort structurel des administrations publiques s'établit comme suit :

(En points de produit intérieur brut potentiel)

<div><div align="center"><table border="1"><tr><th/><th>
2017</th><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="justify" valign="middle">
Effort structurel</td><td align="right" valign="middle">
0,0</td><td align="right" valign="middle">
0,2</td><td align="right" valign="middle">
0,3</td><td align="right" valign="middle">
0,4</td><td align="right" valign="middle">
0,5</td><td align="right" valign="middle">
0,5</td></tr><tr><td align="left" valign="middle">
Dont :</td><td align="left"/><td align="left"/><td align="left"/><td align="left"/><td align="left"/><td align="left"/></tr><tr><td align="left" valign="middle">
- mesures nouvelles sur les prélèvements obligatoires (hors crédits d'impôts)</td><td align="right">
- 0,1</td><td align="right">
- 0,3</td><td align="right">
- 0,1</td><td align="right">
- 0,5</td><td align="right">
0,0</td><td align="right">
0,1</td></tr><tr><td align="left" valign="middle">
- effort en dépense (hors crédits d'impôts)</td><td align="right">
0,0</td><td align="right">
0,4</td><td align="right">
0,4</td><td align="right">
0,5</td><td align="right">
0,5</td><td align="right">
0,6</td></tr><tr><td align="left" valign="middle">
- clé de crédits d'impôts</td><td align="right">
0,1</td><td align="right">
0,0</td><td align="right">
0,0</td><td align="right">
0,4</td><td align="right">
0,0</td><td align="right">
- 0,2</td></tr></table></div></div>
