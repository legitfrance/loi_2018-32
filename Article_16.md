I. - L'ensemble des concours financiers de l'Etat aux collectivités territoriales, exprimés en milliards d'euros courants, est évalué comme suit, à périmètre constant :

<div><div align="center"><table border="1"><tr><th/><th>
2018</th><th>
2019</th><th>
2020</th><th>
2021</th><th>
2022</th></tr><tr><td align="left" valign="middle">
Total des concours financiers de l'Etat aux collectivités territoriales</td><td align="center">
48,11</td><td align="center">
48,09</td><td align="center">
48,43</td><td align="center">
48,49</td><td align="center">
48,49</td></tr><tr><td align="left" valign="middle">
Fonds de compensation pour la taxe sur la valeur ajoutée</td><td align="center">
5,61</td><td align="center">
5,71</td><td align="center">
5,95</td><td align="center">
5,88</td><td align="center">
5,74</td></tr><tr><td align="left" valign="middle">
Taxe sur la valeur ajoutée affectée aux régions</td><td align="center">
4,12</td><td align="center">
4,23</td><td align="center">
4,36</td><td align="center">
4,50</td><td align="center">
4,66</td></tr><tr><td align="left" valign="middle">
Autres concours</td><td align="center">
38,37</td><td align="center">
38,14</td><td align="center">
38,12</td><td align="center">
38,10</td><td align="center">
38,10</td></tr></table></div></div>
II. - Cet ensemble est constitué par :
1° Les prélèvements sur recettes de l'Etat établis au profit des collectivités territoriales ;
2° Les crédits du budget général relevant de la mission « Relations avec les collectivités territoriales » ;
3° Le produit de l'affectation de la taxe sur la valeur ajoutée aux régions, au département de Mayotte et aux collectivités territoriales de Corse, de Martinique et de Guyane prévue à l'article 149 de la loi n° 2016-1917 du 29 décembre 2016 de finances pour 2017.
III. - Pour la durée de la programmation, l'ensemble des concours financiers autres que le Fonds de compensation pour la taxe sur la valeur ajoutée prévu à l'article L. 1615-1 du code général des collectivités territoriales et que le produit de l'affectation de la taxe sur la valeur ajoutée aux régions, au département de Mayotte et aux collectivités territoriales de Corse, de Martinique et de Guyane prévue à l'article 149 de la loi n° 2016-1917 du 29 décembre 2016 précitée est plafonné, à périmètre constant, aux montants du tableau du I du présent article.
